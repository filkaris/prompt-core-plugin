<?php

class Prompt_MailTestCase extends WP_UnitTestCase {

	/**
	 * @var callback $mail_test
	 */
	protected $mail_test;

	protected $mail_test_data;

	function setUp() {
		parent::setUp();

		$this->mail_test = null;
		$this->mail_test_data = null;

		add_filter( 'phpmail_init', array( $this, 'prevent_phpmail_sending' ) );
		add_filter( 'wp_mail', array( $this, 'mail_filter' ) );
	}

	function tearDown() {
		remove_filter( 'wp_mail', array( $this, 'mail_filter' ) );
		remove_filter( 'phpmail_init', array( $this, 'prevent_phpmail_sending' ) );
	}

	function prevent_phpmail_sending( PHPMailer $phpmailer ) {
		$mock = $this->getMock( 'PHPMailer' );
		$mock->expects( $this->once() )
			->method( 'send' )
			->will( $this->returnValue( true ) );
	}

	function mail_filter( $mail ) {
		call_user_func( $this->mail_test, $mail );

		// Always check the message for errors
		$this->assertNotContains( 'Error: ', $mail['message'] );
		$this->assertNotContains( 'Warning: ', $mail['message'] );
		$this->assertNotContains( 'Notice: ', $mail['message'] );

		return $mail;
	}

}