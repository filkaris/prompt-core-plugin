<?php

class SkimlinksOptionsTabTest extends WP_UnitTestCase {

	function testRender() {
		$tab = new Prompt_Admin_Skimlinks_Options_Tab( Prompt_Core::$options );

		$content = $tab->render();

		$this->assertContains( 'skimlinks_publisher_id', $content );
	}

	function testValidate() {

		$old_data = array(
			'skimlinks_publisher_id' => '',
		);

		$new_data = array(
			'skimlinks_publisher_id' => 'TEST',
		);

		$tab = new Prompt_Admin_Skimlinks_Options_Tab( Prompt_Core::$options );

		$validated_data = $tab->validate( $new_data, $old_data );

		$this->assertEmpty( array_diff_assoc( $new_data, $validated_data ), 'Did not get expected validated data.' );
	}

}
