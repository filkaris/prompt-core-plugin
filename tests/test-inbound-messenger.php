<?php

class InboundMessengerTest extends Prompt_MockMailerTestCase {
	/** @var WP_User */
	protected $commenter = null;
	/** @var WP_Post */
	protected $post = null;
	/** @var Prompt_Inbound_Messenger */
	protected $messenger = null;

	function setUp() {
		parent::setUp();
		$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
		$_SERVER['SERVER_NAME'] = 'localhost';
		$this->commenter = $this->factory->user->create_and_get();
		$this->post = $this->factory->post->create_and_get( array(
			'post_author' => $this->factory->user->create(),
		) );
		$this->messenger = Prompt_Factory::make_inbound_messenger();
	}

	function testInboundComment() {

		$restore_whitelist = get_option( 'comment_whitelist' );
		update_option( 'comment_whitelist', false );

		$command = new Prompt_Comment_Command();
		$command->set_post_id( $this->post->ID );
		$command->set_user_id( $this->commenter->ID );

		$update = new stdClass();
		$update->id = 'testid';
		$update->type = 'inbound-email';
		$update->status = 'accepted';
		$update->data = new stdClass();
		$update->data->from = 'from@test.dom';
		$update->data->message = <<<EOD
I'm an email comment. This is my text.

It has a couple of lines. The next text is quoted from the original email.
EOD;
		$update->data->metadata = new stdClass();
		$update->data->metadata->ids = array_merge( array( 1 ), $command->get_keys() );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->never() )->method( 'send_one' );

		$this->messenger->process_update( $update );

		$comments = get_approved_comments( $this->post->ID );
		$this->assertCount( 1, $comments, 'Comment wasn\'t added.' );
		$this->assertEquals( $this->commenter->ID, $comments[0]->user_id, 'Comment posted from the wrong user.' );
		$this->assertNotContains( 'INTRO TEXT', $comments[0]->comment_content, 'Quoted email not stripped from comment.' );

		update_option( 'comment_whitelist', $restore_whitelist );
	}

	function verifySubscribedEmail( Prompt_Email $email ) {
		$this->assertEquals( $this->commenter->user_email, $email->get_to_address() );
		$this->assertContains( ' subscribed', $email->get_subject() );
	}

	function testCommentSubscribe() {

		$command = new Prompt_Comment_Command();
		$command->set_post_id( $this->post->ID );
		$command->set_user_id( $this->commenter->ID );

		$update = new stdClass();
		$update->id = 'testid';
		$update->type = 'inbound-email';
		$update->status = 'accepted';
		$update->data = new stdClass();
		$update->data->from = 'from@test.dom';
		$update->data->message = 'subscribe';
		$update->data->metadata = new stdClass();
		$update->data->metadata->ids = array_merge( array( 1 ), $command->get_keys() );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_one' )
			->with( $this->objectHasAttribute( 'subject' ) )
			->will( $this->returnCallback( array( $this, 'verifySubscribedEmail' ) ) );

		$this->messenger->process_update( $update );

		$prompt_post = new Prompt_Post( $this->post );
		$this->assertTrue( $prompt_post->is_subscribed( $this->commenter->ID ), 'Expected commenter to be subscribed.' );

		$comments = get_comments( array( 'post_id' => $prompt_post->id() ) );
		$this->assertEmpty( $comments, 'Expected no comments to be added.' );
	}

	function testCommentUnsubscribe() {
		$prompt_post = new Prompt_Post( $this->post );
		$prompt_post->subscribe( $this->commenter->ID );

		$command = new Prompt_Comment_Command();
		$command->set_post_id( $this->post->ID );
		$command->set_user_id( $this->commenter->ID );

		$update = new stdClass();
		$update->id = 'testid';
		$update->type = 'inbound-email';
		$update->status = 'accepted';
		$update->data = new stdClass();
		$update->data->from = 'from@test.dom';
		$update->data->message = "\nunsubscribe\n\nthanks a lot\n";
		$update->data->metadata = new stdClass();
		$update->data->metadata->ids = array_merge( array( 1 ), $command->get_keys() );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_one' )
			->with( $this->objectHasAttribute( 'subject' ) )
			->will( $this->returnCallback( array( $this, 'verifyUnsubscribedEmail' ) ) );

		$this->messenger->process_update( $update );

		$this->assertFalse( $prompt_post->is_subscribed( $this->commenter->ID ), 'Expected commenter to be unsubscribed.' );
	}

	function verifyUnsubscribedEmail( Prompt_Email $email ) {
		$this->assertEquals( $this->commenter->user_email, $email->get_to_address() );
		$this->assertContains( ' unsubscribed', $email->get_subject() );
	}


	function testPullUpdates() {

		// Already subscribed commenter does not trigger a subscribed email
		$prompt_post = new Prompt_Post( $this->post );
		$prompt_post->subscribe( $this->commenter->ID );

		$command = new Prompt_Comment_Command();
		$command->set_post_id( $this->post->ID );
		$command->set_user_id( $this->commenter->ID );

		$response_body = array(
			'updates' => array(
				array(
					'id' => 'testid',
					'type' => 'inbound-email',
					'status' => 'accepted',
					'data' => array(
						'from' => 'from@test.dom',
						'message' => 'hello world',
						'metadata' => array( 'ids' => array_merge( array( 1 ), $command->get_keys() ) ),
					)
				)
			)
		);

		$response = array(
			'response' => array( 'code' => 200 ),
			'body' => json_encode( $response_body ),
		);

		$mock_client = $this->getMock( 'Prompt_Api_Client' );
		$mock_client->expects( $this->once() )
			->method( 'get' )
			->with( '/updates/undelivered' )
			->will( $this->returnValue( $response ) );

		$messenger = new Prompt_Inbound_Messenger( $mock_client );
		$messenger->pull_updates();
	}

	function testAcknowledgeUpdates() {

		$updated_results_body = array(
			'updates' => array(
				array(
					'id' => 'testid',
					'status' => 'delivered'
				)
			)
		);

		$put_request = array(
			'headers' => array( 'Content-Type' => 'application/json' ),
			'body' => json_encode( $updated_results_body )
		);

		$put_response = array(
			'response' => array( 'code' => 200 ),
			'body' => json_encode( $updated_results_body )
		);

		$mock_client = $this->getMock( 'Prompt_Api_Client' );
		$mock_client->expects( $this->once() )
			->method( 'put' )
			->with( '/updates', $put_request )
			->will( $this->returnValue( $put_response ) );

		$messenger = new Prompt_Inbound_Messenger( $mock_client );
		$result = $messenger->acknowledge_updates( $updated_results_body );

		$this->assertTrue( $result, 'Expected successful acknowledgement.' );
	}

	function testAcknowledgeEmpty() {
		$empty_updates = array( 'updates' => array() );

		$mock_client = $this->getMock( 'Prompt_Api_Client' );
		$mock_client->expects( $this->never() )->method( 'put' );

		$messenger = new Prompt_Inbound_Messenger( $mock_client );
		$result = $messenger->acknowledge_updates( $empty_updates );

		$this->assertTrue( $result, 'Expected success with no request necessary.' );
	}
}