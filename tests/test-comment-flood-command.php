<?php

class PromptCommentFloodCommandTest extends Prompt_MockMailerTestCase {

	/**
	 * @dataProvider rejoinCommandProvider
	 */
	function testRejoin( $command ) {
		$post_id = $this->factory->post->create();
		$this->mail_data->commenter = $this->factory->user->create_and_get();
		$comment_text = $command;

		$message = new stdClass();
		$message->message = $comment_text;

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_one' )
			->with( $this->objectHasAttribute( 'subject' ) )
			->will( $this->returnCallback( array( $this, 'verifyRejoinEmail' ) ) );

		$command = new Prompt_Comment_Flood_Command();
		$command->set_keys( array( $post_id, $this->mail_data->commenter->ID ) );
		$command->set_message( $message );
		$command->execute();

		$prompt_post = new Prompt_Post( $post_id );

		$this->assertTrue( $prompt_post->is_subscribed( $this->mail_data->commenter->ID ), 'Expected user to be subscribed.' );

		$comments = get_comments( array(
			'post_id' => $post_id,
			'user_id' => $this->mail_data->commenter->ID,
		) );
		$this->assertCount( 0, $comments, 'Expected no comments.' );
	}

	function verifyRejoinEmail( Prompt_Email $email ) {
		$this->assertEquals( $this->mail_data->commenter->user_email, $email->get_to_address() );
		$this->assertContains( ' rejoined', $email->get_subject() );
	}

	function rejoinCommandProvider() {
		return array(
			array( 'rejoin' ),
			array( '*rejoin*' ),
			array( 'Rejion' ),
			array( "\t" ),
			array( '' ),
		);
	}

	function testCommentSuppression() {

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->never() )->method( 'send_many' );

		$post_id = $this->factory->post->create();
		$commenter_id = $this->factory->user->create();

		$comment_text = 'Test comment.';
		$message = new stdClass();
		$message->message = $comment_text;

		$command = new Prompt_Comment_Flood_Command();
		$command->set_keys( array( $post_id, $commenter_id ) );
		$command->set_message( $message );
		$command->execute();

		$comments = get_comments( array(
			'post_id' => $post_id,
			'user_id' => $commenter_id,
		) );
		$this->assertCount( 0, $comments, 'Expected no comments.' );

	}
}
