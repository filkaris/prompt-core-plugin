<?php

class ModerationMailingTest extends Prompt_MockMailerTestCase {

	function testAutoApproveModeratorComment() {
		$admin = $this->factory->user->create_and_get( array( 'role' => 'administrator' ) );
		$post = $this->factory->post->create_and_get();
		$comment = $this->factory->comment->create_and_get( array(
			'comment_post_ID' => $post->ID,
			'comment_approved' => 0,
			'comment_author_email' => $admin->user_email,
		) );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->never() )->method( 'send_many' );

		Prompt_Moderation_Mailing::send_notifications( $comment, array( $admin->user_email ) );

		$check_comment = get_comment( $comment->comment_ID );
		$this->assertEquals( 1, $check_comment->comment_approved, 'Expected auto-approved moderator comment.' );
	}

	function testModerationNotifications() {

		$admin = $this->factory->user->create_and_get( array( 'role' => 'administrator' ) );
		$author = $this->factory->user->create_and_get( array( 'role' => 'author' ) );
		$post = $this->factory->post->create_and_get( array( 'post_author' => $author->ID ) );
		$comment = $this->factory->comment->create_and_get( array(
			'comment_post_ID' => $post->ID,
			'comment_approved' => 0,
		) );

		$this->mail_data->admin = $admin;
		$this->mail_data->author = $author;
		$this->mail_data->post = $post;
		$this->mail_data->comment = $comment;
		$this->mail_data->addresses = array( $admin->user_email, $author->user_email );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_many' )
			->will( $this->returnCallback( array( $this, 'verifyModerationNotifications' ) ) );

		Prompt_Moderation_Mailing::send_notifications( $comment, $this->mail_data->addresses  );
	}

	/**
	 * @param Prompt_Email[] $emails
	 */
	function verifyModerationNotifications( array $emails ) {

		$this->assertCount( 2, $emails, 'Expected two notification emails.' );

		$this->assertContains( $emails[0]->get_to_address(), $this->mail_data->addresses );
		$this->assertContains( $emails[1]->get_to_address(), $this->mail_data->addresses );

		$email = $emails[0];

		$this->assertContains(
			$this->mail_data->post->post_title,
			$email->get_subject(),
			'Expected post title in email subject.'
		);

		$this->assertContains(
			$this->mail_data->comment->comment_author,
			$email->get_html(),
			'Expected commenter name in email.'
		);

		$this->assertContains(
			$this->mail_data->comment->comment_content,
			$email->get_html(),
			'Expected comment content in email.'
		);

		$this->assertContains(
			$this->mail_data->comment->comment_ID,
			$email->get_metadata()->ids,
			'Expected comment ID in metadata'
		);

		$this->assertEquals(
			Prompt_Enum_Message_Types::COMMENT_MODERATION,
			$email->get_message_type(),
			'Expected comment moderation message type.'
		);
	}

}