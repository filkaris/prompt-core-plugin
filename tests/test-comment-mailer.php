<?php

class CommentMailerTest extends Prompt_MockMailerTestCase {

	protected $user;
	protected $post;

	public function setUp() {
		parent::setUp();

		// API transport tests both html and text templates
		Prompt_Core::$options->set( 'email_transport', Prompt_Enum_Email_Transports::API );

		$this->post = $this->factory->post->create_and_get();
		$this->user = $this->factory->user->create_and_get();

		// Disable automatic scheduling
		remove_action(
			'wp_insert_comment',
			array( 'Prompt_Outbound_Handling', 'action_wp_insert_comment' ),
			10
		);
		remove_action(
			'transition_comment_status',
			array( 'Prompt_Outbound_Handling', 'action_transition_comment_status' ),
			10
		);

	}

	public function tearDown() {
		Prompt_Core::$options->reset();
		add_action(
			'wp_insert_comment',
			array( 'Prompt_Outbound_Handling', 'action_wp_insert_comment' ),
			10,
			2
		);
		add_action(
			'transition_comment_status',
			array( 'Prompt_Outbound_Handling', 'action_transition_comment_status' ),
			10,
			3
		);
	}

	function getMockFloodController( $comment, $subscriber_ids ) {

		$mock = $this->getMock(
			'Prompt_Comment_Flood_Controller',
			array( 'control_recipient_ids' ),
			array( $comment )
		);
		$mock->expects( $this->once() )
			->method( 'control_recipient_ids' )
			->will( $this->returnValue( $subscriber_ids ) );

		return $mock;
	}

	function testCommentNotification() {
		$this->mail_data->post_subscriber = $this->factory->user->create_and_get();
		$this->mail_data->site_comments_subscriber = $this->factory->user->create_and_get();

		$subscriber_ids = array(
			$this->mail_data->post_subscriber->ID,
			$this->mail_data->site_comments_subscriber->ID,
		);

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_many' )
			->will( $this->returnCallback( array( $this, 'verifyCommentNotifications' ) ) );

		$this->mail_data->comment = $this->factory->comment->create_and_get( array(
			'comment_post_ID' => $this->post->ID,
		) );

		$mock_flood_controller = $this->getMockFloodController( $this->mail_data->comment, $subscriber_ids );

		$mailer = new Prompt_Comment_Mailer( $this->mail_data->comment, $mock_flood_controller );

		$mailer->send_notifications();

		// Repeated calls should have no effect

		$mailer->send_notifications();
	}

	/**
	 * @param Prompt_Email[] $emails
	 */
	function verifyCommentNotifications( array $emails ) {
		$this->assertCount( 2, $emails, 'Expected two notification emails.' );
		$to_addresses = array( $emails[0]->get_to_address(), $emails[1]->get_to_address() );
		$this->assertContains( $this->mail_data->post_subscriber->user_email, $to_addresses );
		$this->assertContains( $this->mail_data->site_comments_subscriber->user_email, $to_addresses );
		$this->assertContains( $this->mail_data->comment->comment_author, $emails[0]->get_html() );
		$this->assertContains( $this->mail_data->comment->comment_author, $emails[0]->get_text() );
		$this->assertNotEmpty( $emails[0]->get_metadata(), 'Expected command metadata.' );
		$this->assertNotContains( 'Notice:', $emails[0]->get_html(), 'Expected no notices.' );
		$this->assertNotContains( 'Error:', $emails[0]->get_html(), 'Expected no errors.' );
		$this->assertNotContains( 'Notice:', $emails[0]->get_text(), 'Expected no notices.' );
		$this->assertNotContains( 'Error:', $emails[0]->get_text(), 'Expected no errors.' );
		$this->assertEquals(
			Prompt_Enum_Message_Types::COMMENT,
			$emails[0]->get_message_type(),
			'Expected comment message type.'
		);
	}

	function testNoNotificationForEmptyComment() {
		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->never() )->method( 'send_many' );

		$comment = $this->factory->comment->create_and_get( array(
			'comment_post_ID' => $this->post->ID,
			'comment_content' => '',
		) );

		$mock_flood_controller = $this->getMock( 'Prompt_Comment_Flood_Controller', array( 'control_recipient_ids' ), array( $comment ) );
		$mock_flood_controller->expects( $this->never() )->method( 'control_recipient_ids' );

		$mailer = new Prompt_Comment_Mailer( $comment, $mock_flood_controller );

		$mailer->send_notifications();
	}

	function testAnonymousCommentNotification() {
		$this->mail_data->subscriber = $this->factory->user->create_and_get();
		$post_id = $this->factory->post->create();

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_many' )
			->will( $this->returnCallback( array( $this, 'verifyAnonymousCommentNotifications' ) ) );

		$this->mail_data->comment = $this->factory->comment->create_and_get( array(
			'comment_post_ID' => $post_id,
			'comment_author' => '',
			'comment_author_email' => '',
		) );

		$mock_flood_controller = $this->getMockFloodController(
			$this->mail_data->comment,
			array( $this->mail_data->subscriber->ID )
		);

		$mailer = new Prompt_Comment_Mailer( $this->mail_data->comment, $mock_flood_controller );

		$mailer->send_notifications();
	}

	/**
	 * @param Prompt_Email[] $emails
	 */
	function verifyAnonymousCommentNotifications( array $emails ) {
		$this->assertCount( 1, $emails, 'Expected one notification email.' );
		$this->assertEquals( $this->mail_data->subscriber->user_email, $emails[0]->get_to_address() );
		$this->assertContains( __( 'Anonymous' ), $emails[0]->get_html() );
		$this->assertContains( __( 'Anonymous' ), $emails[0]->get_text() );
	}

	function testCommentReplyNotification() {
		$this->mail_data->parent_author = $this->factory->user->create_and_get();
		$child_author = $this->factory->user->create_and_get();
		$post_id = $this->factory->post->create();

		$parent_comment = array(
			'user_id' => $this->mail_data->parent_author->ID,
			'comment_post_ID' => $post_id,
			'comment_content' => 'test comment',
			'comment_agent' => 'Prompt',
			'comment_author' => $this->mail_data->parent_author->display_name,
			'comment_author_IP' => '',
			'comment_author_url' => $this->mail_data->parent_author->user_url,
			'comment_author_email' => $this->mail_data->parent_author->user_email,
		);

		$parent_comment_id = wp_insert_comment( $parent_comment );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_many' )
			->will( $this->returnCallback( array( $this, 'verifyCommentReplyNotifications' ) ) );

		$child_comment = array(
			'user_id' => $child_author->ID,
			'comment_post_ID' => $post_id,
			'comment_parent' => $parent_comment_id,
			'comment_content' => 'test reply',
			'comment_agent' => 'Prompt',
			'comment_author' => $child_author->display_name,
			'comment_author_IP' => '',
			'comment_author_url' => $child_author->user_url,
			'comment_author_email' => $child_author->user_email,
		);

		$this->mail_data->child_comment_id = wp_insert_comment( $child_comment );

		$child_comment = get_comment( $this->mail_data->child_comment_id );

		$mock_flood_controller = $this->getMockFloodController(
			$child_comment,
			array( $this->mail_data->parent_author->ID )
		);

		$mailer = new Prompt_Comment_Mailer( $child_comment, $mock_flood_controller );

		$mailer->send_notifications();
		// Repeated calls should have no effect
		$mailer->send_notifications();
	}

	/**
	 * @param Prompt_Email[] $emails
	 */
	function verifyCommentReplyNotifications( array $emails ) {
		$this->assertCount( 1, $emails, 'Expected one comment notification email.' );
		$this->assertEquals(
			$this->mail_data->parent_author->user_email,
			$emails[0]->get_to_address(),
			'Expected an email to be sent to the parent comment author.'
		);
		$this->assertEquals(
			$this->mail_data->child_comment_id,
			$emails[0]->get_metadata()->ids[3],
			'Expected comment parent comment id in email metadata.'
		);
		$this->assertEquals(
			Prompt_Enum_Message_Types::COMMENT,
			$emails[0]->get_message_type(),
			'Expected comment message type.'
		);
	}

	function testChunking() {

		$subscriber_ids = $this->factory->user->create_many( 30 );

		$post_id = $this->factory->post->create();

		$this->mail_data->comment = $this->factory->comment->create_and_get( array(
			'comment_post_ID' => $post_id,
		) );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_many' )
			->will( $this->returnValue( true ) );

		$mock_flood_controller = $this->getMockFloodController(
			$this->mail_data->comment,
			$subscriber_ids
		);

		$mailer = new Prompt_Comment_Mailer( $this->mail_data->comment, $mock_flood_controller );


		$this->mail_data->verified_chunk_event = false;
		add_filter( 'schedule_event', array( $this, 'verifyChunkEvent' ) );
		$mailer->send_notifications();
		remove_filter( 'schedule_event', array( $this, 'verifyChunkEvent' ) );

		$this->assertTrue( $this->mail_data->verified_chunk_event, 'Expected another mailing to be scheduled.' );
	}

	function verifyChunkEvent( $event ) {
		$this->assertEquals( 'prompt/comment_mailing/send_notifications', $event->hook );
		$this->assertEquals( $this->mail_data->comment->comment_ID, $event->args[0] );

		$this->assertNotEmpty(
			$event->args[1],
			'Expected unsent post IDs signature as second event argument.'
		);

		$this->mail_data->verified_chunk_event = true;

		// No need to actually schedule the event
		return false;
	}

}