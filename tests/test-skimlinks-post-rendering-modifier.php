<?php
/** @group debug */
class SkimlinksPostRenderingModifierTest extends WP_UnitTestCase {

	protected $publisher_id = 'TEST';

	function setUp() {
		parent::setUp();
		Prompt_Core::$options->set( 'enable_skimlinks', true );
		Prompt_Core::$options->set( 'skimlinks_publisher_id', $this->publisher_id );
	}

	function tearDown() {
		Prompt_Core::$options->reset();
	}

	function testLinkReplacement() {

		$home_url = home_url();
		$external_url = 'https://amazon.com';
		$content = sprintf(
			'Local link <a href="%s">home page</a>. External link <a href="%s">amazon</a>.',
			$home_url,
			$external_url
		);

		$modifier = new Prompt_Skimlinks_Post_Rendering_Modifier();

		$modifier->setup();

		$the_content = apply_filters( 'the_content', $content );
		$redirect_url = htmlentities(
			"http://go.redirectingat.com/?id={$this->publisher_id}&xs=1&url=" . urlencode( $external_url )
		);

		$this->assertContains( $home_url, $the_content );
		$this->assertNotContains( $external_url, $the_content );
		$this->assertContains( $redirect_url, $the_content );

		$modifier->reset();

		$the_content = apply_filters( 'the_content', $content );
		$this->assertContains( $home_url, $the_content );
		$this->assertContains( $external_url, $the_content );
		$this->assertNotContains( $redirect_url, $the_content );
	}
}
