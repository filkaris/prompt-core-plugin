<?php

/**
 * TODO: Move all options testing to the relevant implementation
 */
class OptionsTest extends WP_UnitTestCase {

	function testAugmentCommentForm() {
		Prompt_Core::$options->set( 'prompt_key', 'test' );
		Prompt_Core::$options->set( 'augment_comment_form', true );

		$post_id = $this->factory->post->create();

		ob_start();
		comment_form( '', $post_id );
		$content = ob_get_clean();

		$this->assertContains(
			Prompt_Comment_Form_Handling::SUBSCRIBE_CHECKBOX_NAME,
			$content,
			'Expected prompt checkbox in comment form content.'
		);
		Prompt_Core::$options->reset();
	}

	function testNoAugmentCommentForm() {
		Prompt_Core::$options->set( 'augment_comment_form', false );

		$post_id = $this->factory->post->create();

		ob_start();
		comment_form( '', $post_id );
		$content = ob_get_clean();

		$this->assertNotContains(
			Prompt_Comment_Form_Handling::SUBSCRIBE_CHECKBOX_NAME,
			$content,
			'Expected no prompt checkbox in comment form content.'
		);
		Prompt_Core::$options->reset();
	}

}

