<?php

class InviteOptionsTabTest extends WP_UnitTestCase {

	protected $schedule_verified;

	function setUp() {
		parent::setUp();
		$this->schedule_verified = false;
	}

	function testRender() {
		$tab = new Prompt_Admin_Invite_Options_Tab( Prompt_Core::$options );

		$content = $tab->render();

		$this->assertContains( 'invite_subject', $content, 'Expected the tab to have a subject input.' );
		$this->assertContains( 'loading-indicator', $content, 'Expected the tab to have a loading indicator.' );
		$this->assertContains(
			esc_attr( Prompt_Core::$options->get( 'invite_subject' ) ),
			$content,
			'Expected to find the invite subject.'
		);
		$this->assertContains(
			esc_textarea( Prompt_Core::$options->get( 'invite_introduction' ) ),
			$content,
			'Expected to find the invite introduction.'
		);
	}

	function checkSendAgreementsEvent( $event ) {
		if ( 'prompt/subscription_mailing/send_agreements' != $event->hook )
			return $event;

		$recipients = explode( "\n", $_POST['recipients'] );
		$this->assertInstanceOf( 'Prompt_Site', $event->args[0], 'Expected a site object as the first argument.' );

		$this->assertCount( 2, $event->args[1], 'Expected two recipients.' );

		$this->assertEquals(
			$recipients[0],
			$event->args[1][0]['user_email'],
			'Expected recipient email in second argument.'
		);
		$this->assertEquals(
			$recipients[1],
			$event->args[1][1]['user_email'],
			'Expected recipient email in second argument.'
		);

		$this->assertEquals(
			$_POST['invite_subject'],
			$event->args[2]['subject'],
			'Expected subject in third argument.'
		);
		$this->assertContains(
			$_POST['invite_introduction'],
			$event->args[2]['invite_introduction'],
			'Expected introduction text in third argument.'
		);
		$this->assertContains(
			Prompt_Enum_Message_Types::INVITATION,
			$event->args[2]['message_type'],
			'Expected invitation message type in third argument.'
		);
		$this->assertContains(
			wp_get_current_user()->display_name,
			$event->args[2]['from_name'],
			'Expected the logged in user name in the from name.'
		);

		$this->schedule_verified = true;

		return false;
	}

	function testSend() {

		$admin_user = $this->factory->user->create_and_get( array( 'role' => 'administrator' ) );
		wp_set_current_user( $admin_user->ID );
		add_filter( 'schedule_event', array( $this, 'checkSendAgreementsEvent' ) );

		$_POST['recipients'] = "test1@test.dom\ntest2@test.com";
		$_POST['invite_subject'] = 'Test subject';
		$_POST['invite_introduction'] = 'Test message';

		$tab = $this->getMock( 'Prompt_Admin_Invite_Options_Tab', array( 'add_notice' ), array( Prompt_Core::$options ) );
		$tab->expects( $this->once() )
			->method( 'add_notice' )
			->with( $this->isType( 'string' ), $this->stringContains( 'updated' ) );

		$tab->form_handler();

		$this->assertTrue( $this->schedule_verified, 'Expected to detect the send agreements hook.' );

		remove_filter( 'schedule_event', array( $this, 'checkSendAgreementsEvent' ) );
		wp_set_current_user( 0 );
	}

	function testInvalidEmail() {

		$tab = $this->getMock( 'Prompt_Admin_Invite_Options_Tab', array( 'add_notice' ), array( Prompt_Core::$options ) );
		$tab->expects( $this->once() )->method( 'add_notice' )->with( $this->stringContains( 'Invalid email' ) );

		$tab->schedule_invites( array( 'blargh' ), 'subject', 'message' );
	}

	function testDuplicateEmail() {

		$recipients = array( "test1@test.dom", "test2@test.com", "test1@test.dom" );

		$tab = $this->getMock( 'Prompt_Admin_Invite_Options_Tab', array( 'add_notice' ), array( Prompt_Core::$options ) );
		$tab->expects( $this->exactly( 2 ) )->method( 'add_notice' );

		$tab->schedule_invites( $recipients, 'subject', 'message' );
	}
}