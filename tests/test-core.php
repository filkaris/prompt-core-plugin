<?php

class CoreTest extends WP_UnitTestCase {

	function testOptions() {
		$this->assertNotEmpty( Prompt_Core::$options );
	}

	function testVersion() {
		$short_version = Prompt_Core::version();
		$long_version = Prompt_Core::version( $full = true );

		$this->assertNotEmpty( $short_version, 'Expected a non-empty version.' );
		$this->assertGreaterThanOrEqual( $short_version, $long_version, 'Expected a longer full version.' );
		$this->assertStringStartsWith( $short_version, $long_version, 'Expected compatible versions.' );
	}
}