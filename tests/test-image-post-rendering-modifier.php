<?php

class ImagePostRenderingModifierTest extends WP_UnitTestCase {

	function testPhotonFeaturedImageStripping() {

		$featured_image_src = array(
			'http://i2.wp.com/example.com/files/2015/09/your-art-gallery.jpg?resize=800%2C542',
			false,
			false
		);

		$modifier = new Prompt_Image_Post_Rendering_Modifier( $featured_image_src );

		$modifier->setup();

		$in_post_image_url = 'http://i1.wp.com/example.com/files/2015/09/your-art-gallery-800x610.jpg?w=709';
		$test_content = '<p><img class="aligncenter size-full wp-image-x" src="' .
			$in_post_image_url .
			'" alt="test" data-recalc-dims="1" /></p>';

		$this->assertNotContains(
			'<img',
			apply_filters( 'the_content', $test_content ),
			'Expected featured image to be stripped.'
		);

		$modifier->reset();
	}

}
