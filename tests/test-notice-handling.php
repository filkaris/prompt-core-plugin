<?php

class NoticeHandlingTest extends Prompt_MockMailerTestCase {

	function testNoNoticeByDefault() {
		ob_start();
		Prompt_Admin_Notice_Handling::display();
		$this->assertEmpty( ob_get_clean(), 'Expected no output.' );
	}

	function testUpgradeNotice() {
		$admin_id = $this->factory->user->create( array( 'role' => 'administrator' ) );
		wp_set_current_user( $admin_id );
		Prompt_Core::$options->set( 'upgrade_required', true );

		ob_start();
		Prompt_Admin_Notice_Handling::display();
		$content = ob_get_clean();

		$this->assertContains( 'plugin_status=upgrade', $content, 'Expected a link to plugin upgrades.' );

		Prompt_Core::$options->reset();
		wp_set_current_user( 0 );
	}
}
