<?php

class OptionsOptionsTabTest extends WP_UnitTestCase {

	function testRender() {
		$tab = new Prompt_Admin_Options_Options_Tab( Prompt_Core::$options );

		$content = $tab->render();

		$this->assertContains( 'auto_subscribe_authors', $content );
		$this->assertContains( 'send_login_info', $content );
		$this->assertContains( 'no_post_email_default', $content );
		$this->assertContains( 'excerpt_default', $content );
		$this->assertContains( 'comment_opt_in_default', $content );
		$this->assertContains( 'comment_flood_control_trigger_count', $content );
		$this->assertContains( 'enable_optins', $content );
		$this->assertContains( 'subscribed_introduction', $content );
		$this->assertNotContains( 'enable_skimlinks', $content );
	}

	function testSkimlinksRender() {

		$test_message_types = Prompt_Core::$options->get( 'enabled_message_types' );
		$test_message_types = array_merge( $test_message_types, array( Prompt_Enum_Message_Types::COMMENT_MODERATION ) );
		Prompt_Core::$options->set( 'enabled_message_types', $test_message_types );

		$tab = new Prompt_Admin_Options_Options_Tab( Prompt_Core::$options );

		$content = $tab->render();

		$this->assertContains( 'enable_skimlinks', $content );

		Prompt_Core::$options->reset();
	}

	function testValidate() {

		$old_data = array(
			'auto_subscribe_authors' => true,
			'comment_flood_control_trigger_count' => 6,
		);

		$new_data = array(
			'send_login_info' => 'value',
			'no_post_email_default' => 'value',
			'comment_flood_control_trigger_count' => '8',
			'subscribed_introduction' => '<p>MESSAGE</p>',
		);

		$tab = new Prompt_Admin_Options_Options_Tab( Prompt_Core::$options );

		$validated_data = $tab->validate( $new_data, $old_data );

		$expected_data = array(
			'send_login_info' => true,
			'no_post_email_default' => true,
			'auto_subscribe_authors' => false,
			'excerpt_default' => false,
			'comment_opt_in_default' => false,
			'comment_flood_control_trigger_count' => 8,
			'enable_optins' => false,
			'enable_skimlinks' => false,
			'subscribed_introduction' => '<p>MESSAGE</p>',
		);

		$this->assertEmpty( array_diff_assoc( $expected_data, $validated_data ), 'Did not get expected validated data.' );
	}

}
