<?php

class PromptAjaxTest extends WP_Ajax_UnitTestCase {

	protected $_author = null;
	protected $_post = null;
	/** @var Prompt_Mailer  */
	protected $_mock_mailer = null;

	public function setUp() {
		parent::setUp();
		$this->_author = $this->factory->user->create_and_get();
		$this->_post = $this->factory->post->create_and_get( array(
			'post_author' => $this->_author->ID,
		) );

		// Trying to avoid triggering updates
		remove_filter( 'admin_init', '_maybe_update_core' );
		remove_filter( 'admin_init', '_maybe_update_themes' );
		remove_filter( 'admin_init', '_maybe_update_plugins' );
	}

	public function tearDown() {
		parent::tearDown();
		// Exception cases leave an output buffer on
		if ( ob_get_level() > 1 )
			ob_end_flush();
	}

	public function testAsAdministrator() {
		$this->_setRole( 'administrator' );

		$this->successfulSubscriptionCases();
	}

	public function testAsSubscriber() {
		$this->_setRole( 'subscriber' );

		$this->successfulSubscriptionCases();
	}

	private function successfulSubscriptionCases() {
		$subscription_objects = array(
			new Prompt_Site,
			new Prompt_Post( $this->_post ),
			new Prompt_User( $this->_author ),
		);
		$subscriber = wp_get_current_user();

		foreach ( $subscription_objects as $object ) {
			add_filter( 'prompt/make_mailer', array( $this, 'get_mock_mailer' ) );

			$_POST['subscribe_nonce'] = wp_create_nonce( Prompt_Ajax_Handling::AJAX_NONCE );
			$_POST['subscribe_topic'] = '';
			$_POST['object_type'] = get_class( $object );
			$_POST['object_id'] = $object->id();
			$_POST['subscribe_submit'] = 'subscribe';

			$this->_mock_mailer = $this->getMock( 'Prompt_Mailer' );
			$this->_mock_mailer->expects( $this->once() )
				->method( 'send_one' )
				->with( $this->objectHasAttribute( 'subject' ) )
				->will( $this->returnCallback( array( $this, 'verify_subscribe_email' ) ) );


			try {
				$this->_last_response = '';
				$this->_handleAjax( Prompt_Subscribing::SUBSCRIBE_ACTION );
			} catch ( WPAjaxDieContinueException $e ) {
				unset( $e );
			}

			$this->assertNotEmpty( $this->_last_response );
			$this->assertNotContains( 'Notice:', $this->_last_response );
			$this->assertNotContains( 'Error:', $this->_last_response );
			$this->assertTrue(
				$object->is_subscribed( $subscriber->ID ),
				'Expected successful subscription by ' . get_class( $object )
			);

			$_POST['subscribe_submit'] = 'unsubscribe';

			$this->_mock_mailer = $this->getMock( 'Prompt_Mailer' );
			$this->_mock_mailer->expects( $this->once() )
				->method( 'send_one' )
				->with( $this->objectHasAttribute( 'subject' ) )
				->will( $this->returnCallback( array( $this, 'verify_unsubscribe_email' ) ) );

			try {
				$this->_last_response = '';
				$this->_handleAjax( Prompt_Subscribing::SUBSCRIBE_ACTION );
			} catch ( WPAjaxDieContinueException $e ) {
				unset( $e );
			}

			$this->assertNotEmpty( $this->_last_response );
			$this->assertNotContains( 'Notice:', $this->_last_response );
			$this->assertNotContains( 'Error:', $this->_last_response );
			$this->assertFalse(
				$object->is_subscribed( get_current_user_id() ),
				'Unsubscription failed.'
			);

			remove_filter( 'prompt/make_mailer', array( $this, 'get_mock_mailer' ) );
		}
	}

	public function verify_subscribe_email( Prompt_Email $email ) {
		$this->assertContains( 'subscribe', $email->get_subject() );
		$this->assertNotContains( 'unsubscribe', $email->get_subject() );
		return false;
	}

	public function verify_unsubscribe_email( Prompt_Email $email ) {
		$this->assertContains( 'unsubscribe', $email->get_subject() );
		return false;
	}

	public function testNewUser() {

		wp_logout();

		add_filter( 'prompt/make_mailer', array( $this, 'get_mock_mailer' ) );

		$test_email = 'testuser@prompt.vern.al';
		$_POST['subscribe_nonce'] = wp_create_nonce( Prompt_Ajax_Handling::AJAX_NONCE );
		$_POST['subscribe_topic'] = '';
		$_POST['object_type'] = 'Prompt_Post';
		$_POST['subscribe_name'] = '';
		$_POST['subscribe_email'] = $test_email;
		$_POST['object_id'] = $this->_post->ID;
		$_POST['subscribe_submit'] = 'subscribe';

		$this->_mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->_mock_mailer->expects( $this->once() )
			->method( 'send_one' )
			->with( $this->objectHasAttribute( 'subject' ) )
			->will( $this->returnCallback( array( $this, 'verify_new_user_email' ) ) );

		try {
			$this->_handleAjax( Prompt_Subscribing::SUBSCRIBE_ACTION );
		} catch ( WPAjaxDieContinueException $e ) {
			unset( $e );
		}

		$this->assertNotEmpty( $this->_last_response );
		$this->assertNotContains( 'Notice:', $this->_last_response );
		$this->assertNotContains( 'Error:', $this->_last_response );

		$new_user = get_user_by( 'email', $test_email );
		$this->assertEmpty( $new_user, 'Did not expect the new user to be created yet.' );

		$prompt_post = new Prompt_Post( $this->_post );
		$this->assertCount( 0, $prompt_post->subscriber_ids(), 'Expected the post to have no subscribers.' );

		remove_filter( 'prompt/make_mailer', array( $this, 'get_mock_mailer' ) );
	}

	public function verify_new_user_email( Prompt_Email $email ) {
		$this->assertContains( 'verify', $email->get_subject() );
		return false;
	}

	public function testLoggedOutSubscribedUser() {

		$user = $this->factory->user->create_and_get();

		$prompt_site = new Prompt_Site();

		$prompt_site->subscribe( $user->ID );

		wp_logout();

		add_filter( 'prompt/make_mailer', array( $this, 'get_mock_mailer' ) );

		$_POST['subscribe_nonce'] = wp_create_nonce( Prompt_Ajax_Handling::AJAX_NONCE );
		$_POST['subscribe_topic'] = '';
		$_POST['object_type'] = 'Prompt_Site';
		$_POST['subscribe_name'] = 'TEST';
		$_POST['subscribe_email'] = $user->user_email;
		$_POST['object_id'] = $prompt_site->id();
		$_POST['subscribe_submit'] = 'subscribe';

		$this->_mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->_mock_mailer->expects( $this->never() )->method( 'send_one' );

		try {
			$this->_handleAjax( Prompt_Subscribing::SUBSCRIBE_ACTION );
		} catch ( WPAjaxDieContinueException $e ) {
			unset( $e );
		}

		$this->assertNotEmpty( $this->_last_response );
		$this->assertNotContains( 'Notice:', $this->_last_response );
		$this->assertNotContains( 'Error:', $this->_last_response );
		$this->assertContains( 'already', $this->_last_response );

		$this->assertTrue( $prompt_site->is_subscribed( $user->ID ), 'Expected the user to still be subscribed.' );

		remove_filter( 'prompt/make_mailer', array( $this, 'get_mock_mailer' ) );
	}

	public function testBadNonce() {
		$_POST['subscribe_nonce'] = 'wrong';
		$_POST['subscribe_topic'] = '';
		$_POST['object_type'] = 'Prompt_User';
		$_POST['object_id'] = $this->_author;
		$_POST['subscribe_submit'] = 'subscribe';

		$this->setExpectedException( 'PHPUnit_Framework_Error' );
		$this->_handleAjax( Prompt_Subscribing::SUBSCRIBE_ACTION );
	}

	public function testBadTopic() {
		$_POST['subscribe_nonce'] = wp_create_nonce( Prompt_Ajax_Handling::AJAX_NONCE );
		$_POST['subscribe_topic'] = 'gotcha';
		$_POST['object_type'] = 'Prompt_User';
		$_POST['object_id'] = $this->_author;
		$_POST['subscribe_submit'] = 'subscribe';

		$this->setExpectedException( 'PHPUnit_Framework_Error' );
		$this->_handleAjax( Prompt_Subscribing::SUBSCRIBE_ACTION );
	}

	public function testCommentUnsubscribe() {
		$commenter_id = $this->factory->user->create();
		$post_id = $this->factory->post->create();

		$prompt_post = new Prompt_Post( $post_id );
		$prompt_post->subscribe( $commenter_id );

		wp_set_current_user( $commenter_id );

		$_POST['nonce'] = wp_create_nonce( Prompt_Ajax_Handling::AJAX_NONCE );
		$_POST['post_id'] = $post_id;

		try {
			$this->_handleAjax( Prompt_Comment_Form_Handling::UNSUBSCRIBE_ACTION );
		} catch ( WPAjaxDieContinueException $e ) {
			unset( $e );
		}

		$this->assertFalse( $prompt_post->is_subscribed( $commenter_id ) );
	}

	public function testInviteUsers() {
		try {
			$this->_handleAjax( 'prompt_get_invite_users' );
		} catch ( WPAjaxDieContinueException $e ) {
			unset( $e );
		}

		$data = json_decode( $this->_last_response );

		$this->assertNotEmpty( $data, 'Expected user data.' );

		$check_user = false;
		foreach ( $data as $user ) {
			if ( $user->address == $this->_author->user_email )
				$check_user = $user;
		}

		$this->assertEquals( $this->_author->display_name, $check_user->name );
		$this->assertEquals( $this->_author->roles, $check_user->roles );
		$this->assertFalse( $check_user->is_post_subscriber, 'Expected author to be a post subscriber.' );
	}

	public function testPreview() {

		add_filter( 'prompt/make_mailer', array( $this, 'get_mock_mailer' ) );

		$_GET['post_id'] = $this->_post->ID;

		$this->_mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->_mock_mailer->expects( $this->once() )
			->method( 'send_one' )
			->with( $this->objectHasAttribute( 'subject' ) )
			->will( $this->returnCallback( array( $this, 'verifyPreviewEmail' ) ) );

		try {
			$this->_handleAjax( 'prompt_post_delivery_preview' );
		} catch ( WPAjaxDieContinueException $e ) {
			unset( $e );
		}

		$this->assertNotEmpty( $this->_last_response );
		$this->assertNotContains( 'Notice:', $this->_last_response );
		$this->assertNotContains( 'Error:', $this->_last_response );

		$this->assertContains( 'Preview email sent', $this->_last_response );

		remove_filter( 'prompt/make_mailer', array( $this, 'get_mock_mailer' ) );
	}

	public function verifyPreviewEmail( Prompt_Email $email ) {
		$current_user = wp_get_current_user();
		$this->assertEquals( $current_user->user_email, $email->get_to_address() );
		$this->assertContains( $this->_post->post_title, $email->get_subject() );
		$this->assertContains( $this->_post->post_content, $email->get_html() );
		return false;
	}

	public function get_mock_mailer() {
		return $this->_mock_mailer;
	}
}