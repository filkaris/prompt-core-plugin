<?php

class EmailTemplateTest extends WP_UnitTestCase {

	function testCommentHeader() {

		Prompt_Core::$options->set( 'email_transport', Prompt_Enum_Email_Transports::API );

		$template = new Prompt_Email_Template( 'test-email.php' );

		$content = $template->render( array( 'comment_header' => true ) );

		$this->assertContains( 'class="header wrap commentheader"', $content, 'Expected the comment header.' );

		Prompt_Core::$options->reset();
	}

	function testPostHeader() {
		Prompt_Core::$options->set( 'email_transport', Prompt_Enum_Email_Transports::API );

		$template = new Prompt_Email_Template( 'test-email.php' );

		$content = $template->render( array() );

		$this->assertContains( 'class="header wrap"', $content, 'Expected the post header.' );

		Prompt_Core::$options->reset();
	}
}
