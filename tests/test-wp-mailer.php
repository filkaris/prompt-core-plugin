<?php

class WpMailerTest extends WP_UnitTestCase {
	private $vcard_string;
	private $prepare_reply_to_name = 'Prompt';
	private $prepare_reply_to_address = 'prompt+reply@test.dom';

	function setUp() {
		parent::setUp();
		$this->vcard_string = "BEGIN:VCARD\r\nN:VCard Tester\r\nEMAIL:test@prompt.vern.al\r\n" .
			"TITLE:Test Vcard\r\nEND:VCARD\r\n";
	}

	function test_email() {
		$email_fields = array(
			'to_address' => 'test@prompt.vern.al',
			'to_name' => 'To Name',
			'from_name' => 'From Name',
			'from_address' => 'from@email.org',
			'subject' => 'Test Subject',
			'text' => 'Test Message',
			'reply_name' => 'Reply Name',
			'reply_address' => 'reply@email.org',
			'message_type' => Prompt_Enum_Message_Types::POST,
		);

		$mailer_mock = $this->get_phpmailer_mock(
			$email_fields['to_address'],
			$email_fields['to_name'],
			$email_fields['reply_address'],
			$email_fields['reply_name'],
			$email_fields['message_type']
		);
		$wp_mailer = new Prompt_Wp_Mailer( $this->get_unused_api_mock(), $mailer_mock );

		$this->assertTrue( $wp_mailer->send_one( new Prompt_Email( $email_fields ) ), 'Text email was not sent.' );

		$this->assertEquals( $email_fields['from_address'], $mailer_mock->From, 'Expected original from address.' );
		$this->assertEquals( $email_fields['from_name'], $mailer_mock->FromName, 'Expected original from name.' );
		$this->assertEquals( $email_fields['subject'], $mailer_mock->Subject, 'Expected original subject.' );
		$this->assertContains( $email_fields['text'], $mailer_mock->Body, 'Expected original message.' );
		$this->assertEquals( Prompt_Enum_Content_Types::HTML, $mailer_mock->ContentType, 'Expected HTML content type.' );
	}

	function test_emails_filter() {
		add_filter( 'prompt/outbound/emails', '__return_empty_array' );

		$email = new Prompt_Email( array(
			'to_address' => 'test@example.com',
			'subject' => 'Test Subject',
			'html' => 'Test Message',
		) );

		$phpmailer_mock = $this->getMock( 'PHPMailer' );
		$phpmailer_mock->expects( $this->never() )->method( 'send' );

		$wp_mailer = new Prompt_Wp_Mailer( $this->get_unused_api_mock(), $phpmailer_mock );

		$result = $wp_mailer->send_one( $email );

		$this->assertFalse( $result, 'Expected send to return false.' );

		remove_filter( 'prompt/outbound/emails', '__return_empty_array' );
	}

	function test_metadata_email() {
		$email_fields = array(
			'to_address' => 'test@prompt.vern.al',
			'subject' => 'tracked email',
			'text' => 'test message',
			'metadata' => array( 'key' => 'value' ),
		);

		$mailer_mock = $this->get_phpmailer_mock(
			$email_fields['to_address'],
			'',
			$this->prepare_reply_to_address,
			$this->prepare_reply_to_name
		);

		$wp_mailer = new Prompt_Wp_Mailer( $this->get_prepare_api_mock(), $mailer_mock );

		$this->assertTrue( $wp_mailer->send_one( new Prompt_Email( $email_fields ) ), 'Metadata email was not sent.' );
	}

	function test_file_attachments() {
		$file1_name = wp_tempnam();
		$file1_content = 'file1';
		file_put_contents( $file1_name, $file1_content );
		$file2_name = wp_tempnam();
		$file2_content = $this->vcard_string;
		file_put_contents( $file2_name, $file2_content );

		$email_fields = array(
			'to_address' => 'to@prompt.vern.al',
			'from_name' => 'From Name',
			'from_address' => 'from@email.org',
			'subject' => 'Attachment Subject',
			'text' => 'Attachment Message',
			'file_attachments' => array( $file1_name, $file2_name ),
			'metadata' => array( 1 ),
		);

		$mailer_mock = $this->get_phpmailer_mock(
			$email_fields['to_address'],
			'',
			$this->prepare_reply_to_address,
			$this->prepare_reply_to_name
		);

		$wp_mailer = new Prompt_Wp_Mailer( $this->get_prepare_api_mock(), $mailer_mock );

		$this->assertTrue( $wp_mailer->send_one( new Prompt_Email( $email_fields ) ), 'Attachments email was not sent.' );

		$attachments = $mailer_mock->getAttachments();
		$this->assertCount( 2, $attachments, 'Expected 2 email attachments.' );
		$this->assertEquals( $file1_name, $attachments[0][0], 'Expected original file attachment path.' );
		$this->assertEquals( $file2_name, $attachments[1][0], 'Expected original file attachment path.' );
	}

	function test_prepare_error() {
		$email = new Prompt_Email( array(
			'to_address' => 'test@test.dom',
			'subject' => 'test',
			'text' => 'test message',
			'metadata' => array( 1 ),
		) );

		$prepare_mock = $this->getMock( 'Prompt_Api_Client' );
		$prepare_mock->expects( $this->once() )
			->method( 'post' )
			->with( '/outbound_messages', $this->arrayHasKey( 'body' ) )
			->will( $this->returnValue( new WP_Error( 'test', 'test error' ) ) );

		$wp_mailer = new Prompt_Wp_Mailer( $prepare_mock );

		$result = $wp_mailer->send_one( $email );

		$this->assertInstanceOf( 'WP_Error', $result, 'Expected an error result.' );
	}

	function test_missing_address_error() {
		$email = new Prompt_Email( array(
			'to_address' => '',
			'subject' => 'test',
			'text' => 'test message',
			'metadata' => array( 1 ),
		) );

		$wp_mailer = new Prompt_Wp_Mailer( $this->get_prepare_api_mock() );

		$this->setExpectedException( 'PHPUnit_Framework_Error' );

		$result = $wp_mailer->send_one( $email );

		$this->assertInstanceOf( 'WP_Error', $result, 'Expected an error result.' );
	}

	function dont_test_string_attachments() {

		$this->mail_test_data = new stdClass();
		$this->mail_test_data->string_attachments = array(
			array(
				'string' => 'A text string attachment.',
				'filename' => 'Test content bytes.',
				'encoding' => 'base64',
				'type' => 'application/octet-stream',
			),
			array(
				'string' => $this->vcard_string,
				'filename' => 'test.vcf',
				'encoding' => '8bit',
				'type' => 'text/x-vcard',
			)
		);
		$this->mail_test_data->email_fields = array(
			'to_address' => 'to@prompt.vern.al',
			'from_name' => 'From Name',
			'from_address' => 'from@email.org',
			'subject' => 'String Attachment Subject',
			'message' => 'String Attachment Message',
		);
		$this->mail_test_data->sent = false;

		$email = new Prompt_Email( $this->mail_test_data->email_fields );
		foreach( $this->mail_test_data->string_attachments as $string_attachment ) {
			call_user_func_array( array( $email, 'add_string_attachment' ), $string_attachment );
		}
		$mailer = new Prompt_Mailer( $this->get_unused_api_mock() );
		$this->mail_test = array( $this, 'verify_string_attachments' );

		$mailer->send_one( $email );

		// not sure these will work as we're mocking phpmailer
		$this->assertFalse( $GLOBALS['phpmailer']->IsError(), 'There was a PHPMailer error.' );
		$this->assertCount( 2, $GLOBALS['phpmailer']->GetAttachments(), 'Outgoing mail had no attachments.' );
	}

	function verify_string_attachments( $mail ) {
		// Nothing in the mail reflects the attachments, only PHPMailer sees them
		$this->mail_test_data->sent = true;
	}

	/**
	 * @return Prompt_Api_Client
	 */
	private function get_unused_api_mock() {
		$prepare_mock = $this->getMock( 'Prompt_Api_Client' );
		$prepare_mock->expects( $this->never() )->method( 'send' );
		return $prepare_mock;
	}

	/**
	 * @return Prompt_Api_Client
	 */
	private function get_prepare_api_mock() {
		$prepare_mock = $this->getMock( 'Prompt_Api_Client' );
		$prepare_mock->expects( $this->once() )
			->method( 'post' )
			->with( '/outbound_messages', $this->arrayHasKey( 'body' ) )
			->will( $this->returnCallback( array( $this, 'mock_prepare_response' ) ) );
		return $prepare_mock;
	}

	private function get_phpmailer_mock( $to_address, $to_name = '', $reply_address = '', $reply_name = '', $type = '' ) {
		$mock = $this->getMock( 'PHPMailer', array( 'addAddress', 'addReplyTo', 'addCustomHeader', 'send' ) );

		$mock->expects( $this->once() )
			->method( 'addAddress' )
			->with( $to_address, $to_name );

		if ( $reply_address ) {
			$mock->expects( $this->once() )
				->method( 'addReplyTo' )
				->with( $reply_address, $reply_name );
		} else {
			$mock->expects( $this->never() )
				->method( 'addReplyTo' );
		}


		$unsubscribe_types = array( Prompt_Enum_Message_Types::COMMENT, Prompt_Enum_Message_Types::POST );
		if ( $reply_address and in_array( $type, $unsubscribe_types ) ) {
			$mock->expects( $this->exactly(2) )
				->method( 'addCustomHeader' )
				->withConsecutive(
					array( 'X-Postmatic-Site-URL', home_url() ),
					array( 'List-Unsubscribe', $this->stringStartsWith( '<mailto:' ) )
				);
		} else {
			$mock->expects( $this->once() )
				->method( 'addCustomHeader' )
				->with( 'X-Postmatic-Site-URL', home_url() );
		}

		$mock->expects( $this->once() )
			->method( 'send' );

		return $mock;
	}

	function mock_prepare_response( $endpoint, $request ) {
		$body = json_decode( $request['body'] );

		for ( $i = 0; $i < count( $body->outboundMessages ); $i += 1 ) {
			$body->outboundMessages[$i]->reply_to = $this->prepare_reply_to_name .
				' <' . $this->prepare_reply_to_address . '>';
		}

		return array(
			'response' => array(
				'code' => 200,
			),
			'body' => json_encode( $body ),
		);
	}

}


