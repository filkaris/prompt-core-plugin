<?php

class UserTest extends WP_UnitTestCase {

	/** @var WP_User */
	protected $_wp_user;
	/** @var Prompt_User */
	protected $_prompt_user;

	function setUp() {
		parent::setUp();
		$this->_wp_user = $this->factory->user->create_and_get();
		$this->_prompt_user = new Prompt_User( $this->_wp_user->ID );
	}

	function testObjectConstruction() {
		$prompt_user = new Prompt_User( $this->_wp_user );

		$this->assertEquals( $this->_wp_user->ID, $prompt_user->get_wp_user()->ID, 'Expected equal user IDs.' );
	}

	function testGetId() {
		$this->assertEquals( $this->_wp_user->ID, $this->_prompt_user->id(), 'Expected equal user IDs.' );
	}

	function testSubscriberProfileOptions() {
		$subscriber = $this->factory->user->create_and_get();
		$this->_prompt_user->subscribe( $subscriber->ID );

		$form = $this->_prompt_user->profile_options();

		$this->assertContains( $subscriber->display_name, $form, 'Expected subscriber to be listed.' );
	}

	function testSiteProfileOptions() {
		$form = $this->_prompt_user->profile_options();

		$this->assertContains( 'prompt_site_subscribed', $form, 'Expected site option.' );
		$this->assertContains( 'prompt_site_comments_subscribed', $form, 'Expected site comments option.' );
		$this->assertNotRegExp( '/checked="checked"[^>]*prompt_site_subscribed/', $form, 'Expected unchecked site option.' );
		$this->assertNotRegExp( '/checked="checked"[^>]*prompt_site_comments_subscribed/', $form, 'Expected unchecked site option.' );

		$this->_prompt_user->update_profile_options(  array(
			'prompt_site_subscribed' => true,
			'prompt_site_comments_subscribed' => true
		) );

		$form = $this->_prompt_user->profile_options();

		$this->assertContains( 'prompt_site_subscribed', $form, 'Expected site option.' );
		$this->assertContains( 'prompt_site_comments_subscribed', $form, 'Expected site comments option.' );
		$this->assertNotRegExp( '/checked="checked"[^>]*prompt_site_subscribed/', $form, 'Expected unchecked site option.' );
		$this->assertNotRegExp( '/checked="checked"[^>]*prompt_site_comments_subscribed/', $form, 'Expected unchecked site option.' );

		$current_user_id = get_current_user_id();
		wp_set_current_user( $this->_prompt_user->id() );

		$this->_prompt_user->update_profile_options(  array(
			'prompt_site_subscribed' => true,
			'prompt_site_comments_subscribed' => true
		) );

		$form = $this->_prompt_user->profile_options();

		$this->assertContains( 'prompt_site_subscribed', $form, 'Expected site option.' );
		$this->assertContains( 'prompt_site_comments_subscribed', $form, 'Expected site comments option.' );
		$this->assertRegExp( '/checked="checked"[^>]*prompt_site_subscribed/', $form, 'Expected checked site option.' );
		$this->assertRegExp( '/checked="checked"[^>]*prompt_site_comments_subscribed/', $form, 'Expected checked site option.' );

		wp_set_current_user( $current_user_id );
	}

	function testAuthorProfileOptions() {
		$prompt_author = new Prompt_User( $this->factory->user->create() );
		$prompt_author->subscribe( $this->_prompt_user->id() );

		$form = $this->_prompt_user->profile_options();

		$this->assertContains( 'prompt-author-subscriptions',  $form );
		$this->assertContains( $prompt_author->get_wp_user()->display_name, $form );
	}

	function testSubscriptionUrl() {
		$this->assertEquals(
			get_author_posts_url( $this->_wp_user->ID ),
			$this->_prompt_user->subscription_url(),
			'Expected the author posts URL as subscription URL.'
		);
	}

	function testSubscriptionObjectLabel() {
		$this->assertContains(
			$this->_wp_user->display_name,
			$this->_prompt_user->subscription_object_label(),
			'Expected to see the user display name in the object label.'
		);
	}

	function testSubscriptionDescription() {
		$this->assertContains(
			$this->_wp_user->display_name,
			$this->_prompt_user->subscription_description(),
			'Exptected to see the post title in the description.'
		);
	}

	function testDeleteAllSubscriptions() {
		$prompt_post1 = new Prompt_Post( $this->factory->post->create() );
		$prompt_post2 = new Prompt_Post( $this->factory->post->create() );
		$prompt_user1 = new Prompt_Post( $this->factory->user->create() );
		$prompt_user2 = new Prompt_Post( $this->factory->user->create() );

		$prompt_post1->subscribe( $prompt_user1->id() );
		$prompt_post1->subscribe( $prompt_user2->id() );
		$prompt_post2->subscribe( $prompt_user1->id() );
		$prompt_post2->subscribe( $prompt_user2->id() );

		wp_delete_user( $prompt_user1->id() );

		$this->assertFalse( $prompt_post1->is_subscribed( $prompt_user1->id() ), 'Expected user to be unsubscribed.' );
		$this->assertFalse( $prompt_post2->is_subscribed( $prompt_user1->id() ), 'Expected user to be unsubscribed.' );
		$this->assertTrue( $prompt_post1->is_subscribed( $prompt_user2->id() ), 'Expected user to be subscribed.' );
		$this->assertTrue( $prompt_post2->is_subscribed( $prompt_user2->id() ), 'Expected user to be subscribed.' );
	}

	function testAllSubscriberIds() {
		$user_ids = $this->factory->user->create_many( 8 );

		$prompt_author1 = new Prompt_User( $user_ids[2] );
		$prompt_author2 = new Prompt_User( $user_ids[7] );

		$prompt_author1->subscribe( $user_ids[0] );
		$prompt_author1->subscribe( $user_ids[1] );

		$prompt_author2->subscribe( $user_ids[1] );
		$prompt_author2->subscribe( $user_ids[2] );

		$this->assertEmpty(
			array_diff( array_slice( $user_ids, 0, 3 ), Prompt_User::all_subscriber_ids() ),
			'Expected first three users as author subscribers.'
		);
	}

	function testSetSubscriberOrigin() {
		$origin_data = array(
			'timestamp' => time(),
			'source_label' => 'Test Label',
			'source_url' => 'http://test.postmatic.tld',
			'agreement' => 'test agreement text',
		);

		$this->assertNull( $this->_prompt_user->get_subscriber_origin(), 'Expected null origin to start with.' );

		$this->_prompt_user->set_subscriber_origin( new Prompt_Subscriber_Origin( $origin_data ) );

		$check_user = new Prompt_User( $this->_prompt_user->id() );

		$origin = $check_user->get_subscriber_origin();

		$this->assertEquals( $origin_data['timestamp'], $origin->get_timestamp(), 'Expected original timestamp.' );
		$this->assertEquals( $origin_data['source_label'], $origin->get_source_label(), 'Expected original source label.' );
		$this->assertEquals( $origin_data['source_url'], $origin->get_source_url(), 'Expected original source url.' );
		$this->assertEquals( $origin_data['agreement'], $origin->get_agreement(), 'Expected original agreement.' );
	}

}

