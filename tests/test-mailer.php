<?php

class MailerTest extends WP_UnitTestCase {

	protected $data;

	function test_email_api() {
		$test_fields = array(
			'to_address' => 'test@example.com',
			'from_name' => 'From Name',
			'from_address' => 'from@example.com',
			'subject' => 'Test Subject',
			'html' => 'Test Message',
			'reply_name' => 'Reply Name',
			'reply_address' => 'reply@example.com',
			'message_type' => Prompt_Enum_Message_Types::ADMIN,
		);
		$this->data = $test_fields;

		$email = new Prompt_Email( $test_fields );

		$mailer = new Prompt_Mailer( $this->get_send_mock() );

		$result = $mailer->send_one( $email );
		$this->assertCount( 1, $result->outboundMessages, 'Expected a send confirmation.' );
	}

	function test_emails_filter() {
		add_filter( 'prompt/outbound/emails', '__return_empty_array' );

		$email = new Prompt_Email( array(
			'to_address' => 'test@example.com',
			'subject' => 'Test Subject',
			'html' => 'Test Message',
		) );

		$api_mock = $this->getMock( 'Prompt_Api_Client' );
		$api_mock->expects( $this->never() )->method( 'post' );

		$mailer = new Prompt_Mailer( $api_mock );

		$result = $mailer->send_one( $email );

		$this->assertEmpty( $result->outboundMessages, 'Expected no outbound messages.' );

		remove_filter( 'prompt/outbound/emails', '__return_empty_array' );
	}

	/**
	 * @return Prompt_Api_Client
	 */
	private function get_send_mock() {
		$send_mock = $this->getMock( 'Prompt_Api_Client' );
		$send_mock->expects( $this->once() )
			->method( 'post' )
			->with( '/outbound_messages', $this->arrayHasKey( 'body' ) )
			->will( $this->returnCallback( array( $this, 'mock_send_response' ) ) );
		return $send_mock;
	}

	function mock_send_response( $endpoint, $request ) {
		$body = json_decode( $request['body'] );

		$this->assertNotEmpty( $body->actions, 'Expected an action' );

		$message = $body->outboundMessages[0];
		$this->assertEquals( $this->data['message_type'], $message->type, 'Expected the given message type.' );

		$statuses = array( 'outboundMessages' => array() );
		foreach ( $body->outboundMessages as $email ) {
			$statuses['outboundMessages'][] = array(
				'id' => rand_str( 5 ),
				'status' => 'succeeded',
			);
		}

		return array(
			'response' => array(
				'code' => 200,
			),
			'body' => json_encode( $statuses ),
		);
	}

}