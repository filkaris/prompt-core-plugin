<?php
/**
 * Note that the ajax tests include a lot of stuff that you might expect here.
 * @group debug
 */
class SubscriptionMailingTest extends Prompt_MockMailerTestCase {

	function testAgreement() {
		$this->mail_data->object = new Prompt_Site;
		$this->mail_data->email = 'test@test.dom';
		$user_data = array();

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_one' )
			->with( $this->anything() )
			->will( $this->returnCallback( array( $this, 'verifyAgreement' ) ) );

		Prompt_Subscription_Mailing::send_agreement( $this->mail_data->object, $this->mail_data->email, $user_data );
	}

	function verifyAgreement( Prompt_Email $email ) {
		$this->assertEquals(
			$this->mail_data->email,
			$email->get_to_address(),
			'Expected email to the agreement recipient only.'
		);
		$this->assertContains(
			$this->mail_data->object->subscription_object_label(),
			$email->get_subject(),
			'Expected the object label in the email subject.'
		);
	}

	function testAgreementRetry() {
		add_filter( 'prompt/make_rescheduler', array( $this, 'mockAgreementRescheduler' ) );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_one' )
			->with( $this->anything() );

		$object = new Prompt_Site;
		$email = 'test@test.dom';
		$user_data = array();

		Prompt_Subscription_Mailing::send_agreement( $object, $email, $user_data );

		$this->assertTrue( isset( $this->mail_data->mock_rescheduler ), 'Expected a mock rescheduler to be created.' );

		remove_filter( 'prompt/make_rescheduler', array( $this, 'mockAgreementRescheduler' ) );
	}

	function testAgreementsRetry() {
		add_filter( 'prompt/make_rescheduler', array( $this, 'mockAgreementsRescheduler' ) );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_many' )
			->with( $this->anything() );

		$object = new Prompt_Site;
		$users = array(
			array( 'user_email' => 'test1@test.dom' ),
			array( 'user_email' => 'test2@test.dom' ),
		);
		$template_data = array();

		Prompt_Subscription_Mailing::send_agreements( $object, $users, $template_data );

		$this->assertTrue( isset( $this->mail_data->mock_rescheduler ), 'Expected a mock rescheduler to be created.' );

		remove_filter( 'prompt/make_rescheduler', array( $this, 'mockAgreementsRescheduler' ) );
	}

	function mockAgreementRescheduler( $real_rescheduler ) {
		return $this->mockRescheduler( 'prompt/subscription_mailing/send_agreement' );
	}

	function mockAgreementsRescheduler( $real_rescheduler ) {
		return $this->mockRescheduler( 'prompt/subscription_mailing/send_agreements' );
	}

	function mockRescheduler( $hook ) {
		$mock = $this->getMockBuilder( 'Prompt_Rescheduler' )
			->disableOriginalConstructor()
			->getMock();

		$mock->expects( $this->once() )
			->method( 'found_temporary_error' )
			->will( $this->returnValue( true ) );

		$mock->expects( $this->once() )
			->method( 'reschedule' )
			->with( $this->equalTo( $hook ), $this->anything() );

		$this->mail_data->mock_rescheduler = $mock;
		return $mock;
	}

	function testAgreements() {

		$this->mail_data->object = new Prompt_Site();
		$this->mail_data->users = array(
			array( 'user_email' => 'test1@test.dom' ),
			array( 'user_email' => 'test2@test.dom' ),
		);
		$this->mail_data->template_data = array(
			'subject' => 'Test subject',
			'from_name' => 'Test Author',
			'invite_introduction' => 'Test message',
		);

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_many' )
			->with( $this->anything() )
			->will( $this->returnCallback( array( $this, 'verifyInvites' ) ) );

		Prompt_Subscription_Mailing::send_agreements(
			$this->mail_data->object,
			$this->mail_data->users,
			$this->mail_data->template_data
		);

	}

	/**
	 * @param Prompt_Email[] $emails
	 */
	function verifyInvites( $emails ) {
		$this->assertCount( 2, $emails, 'Expected two invite emails to be sent.' );
		$this->assertEquals(
			$this->mail_data->users[0]['user_email'],
			$emails[0]->get_to_address(),
			'Expected a test recipient.'
		);
		$this->assertEquals( $this->mail_data->template_data['from_name'], $emails[0]->get_from_name() );
		$this->assertEquals(
			$this->mail_data->template_data['subject'],
			$emails[0]->get_subject(),
			'Expected the supplied subject.'
		);
		$this->assertContains(
			$this->mail_data->template_data['invite_introduction'],
			$emails[0]->get_html(),
			'Expected the supplied message.'
		);

		$metadata = $emails[0]->get_metadata();

		$this->assertNotEmpty( $metadata, 'Expected metadata.' );

		$update = new stdClass();
		$update->metadata = $emails[0]->get_metadata();

		$command = Prompt_Command_Handling::make_command( $update );

		$this->assertInstanceOf(
			'Prompt_Register_Subscribe_Command',
			$command,
			'Expected register subscribe command metadata.'
		);

	}

	function testWelcome() {
		Prompt_Core::$options->set( 'email_transport', Prompt_Enum_Email_Transports::API );
		Prompt_Core::$options->set( 'subscribed_introduction', 'XXWELCOMEXX' );

		$object = new Prompt_Site;
		$this->mail_data->object = $object;

		$subscriber = $this->factory->user->create_and_get();
		$this->mail_data->subscriber = $subscriber;

		$object->subscribe( $subscriber->ID );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_one' )
			->with( $this->anything() )
			->will( $this->returnCallback( array( $this, 'verifyWelcome' ) ) );

		Prompt_Subscription_Mailing::send_subscription_notification( $subscriber->ID, $object );

		remove_shortcode( 'testwelcome' );
		Prompt_Core::$options->reset();
	}

	function verifyWelcome( Prompt_Email $email ) {
		$this->assertEquals(
			$this->mail_data->subscriber->user_email,
			$email->get_to_address(),
			'Expected email to the agreement recipient only.'
		);
		$this->assertContains(
			$this->mail_data->object->subscription_object_label(),
			$email->get_subject(),
			'Expected the object label in the email subject.'
		);
		$this->assertContains(
			Prompt_Core::$options->get( 'subscribed_introduction' ),
			$email->get_html(),
			'Expected custom subscribed introduction content in email.'
		);
	}

	function testWelcomeInvalidEmail() {

		$object = new Prompt_Site;
		$subscriber = $this->factory->user->create_and_get( array( 'user_email' => '23kjk3' ) );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->never() )->method( 'send_one' );

		$this->setExpectedException( 'PHPUnit_Framework_Error' );

		Prompt_Subscription_Mailing::send_subscription_notification( $subscriber->ID, $object );
	}

}