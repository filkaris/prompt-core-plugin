<?php

class PostMailingTest extends Prompt_MockMailerTestCase {

	function testPostNotifications() {
		Prompt_Core::$options->set( 'email_transport', Prompt_Enum_Email_Transports::API );
		add_filter( 'the_content', array( $GLOBALS['wp_embed'], 'autoembed' ), 8 );
		add_shortcode( 'unsupportedsc', array( $this, 'unsupportedsc' ) );
		add_shortcode( 'wpgist', array( $this, 'unsupportedsc' ) );
		$this->mail_data->author = $this->factory->user->create_and_get( array( 'role' => 'author' ) );
		wp_set_current_user( $this->mail_data->author->ID );

		$this->mail_data->author_subscriber = $this->factory->user->create_and_get();
		$site_subscriber = $this->factory->user->create_and_get();
		$post_subscriber = $this->factory->user->create_and_get();
		$this->mail_data->title = 'Test & Title';
		$this->mail_data->img_src = 'http://test.tld/image.png';
		$this->mail_data->height_attribute = 'height="200"';
		$this->mail_data->youtube_url = 'https://www.youtube.com/watch?v=JKDo6g4CCeU';
		$this->mail_data->noscript_content = 'scriptless content';
		$post = $this->factory->post->create_and_get( array(
			'post_title' => $this->mail_data->title,
			'post_status' => 'draft',
			'post_author' => $this->mail_data->author->ID,
			'post_content' => 'test content <!--more--> after more <img src="' .
				$this->mail_data->img_src . '" class="test" width="1700" ' .
				$this->mail_data->height_attribute .
				"/> and [unsupportedsc width=\"100\"]inner[/unsupportedsc] shortcode and \n" .
				$this->mail_data->youtube_url . "\n<noscript>" .
				$this->mail_data->noscript_content . "</noscript>\n",
		) );

		$prompt_site = new Prompt_Site;
		$prompt_post = new Prompt_Post( $post->ID );
		$prompt_author = new Prompt_User( $this->mail_data->author->ID );

		$prompt_author->subscribe( $this->mail_data->author_subscriber->ID );
		$prompt_site->subscribe( $site_subscriber->ID );
		$prompt_post->subscribe( $post_subscriber->ID );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_many' )
			->will( $this->returnCallback( array( $this, 'verifyPostNotifications' ) ) );


		Prompt_Post_Mailing::send_notifications( $post );
		remove_shortcode( 'unsupportedsc', array( $this, 'unsupportedsc' ) );
		wp_set_current_user( 0 );
		Prompt_Core::$options->reset();
	}

	function unsupportedsc() {
		return 'I\'m unsupported!';
	}

	/**
	 * @param Prompt_Email[] $emails
	 */
	function verifyPostNotifications( array $emails ) {

		$this->assertCount( 2, $emails, 'Expected two notification emails.' );

		foreach ( $emails as $email ) {
			$this->assertNotContains( 'Error:', $email->get_html(), 'Expected no error notifications.' );
			$this->assertNotContains( 'Warning:', $email->get_html(), 'Expected no warning notifications.' );
			$this->assertContains( $this->mail_data->title, $email->get_subject() );
			$this->assertContains( '<p>', $email->get_html(), 'Expected auto paragraph tag in content.' );
			$this->assertNotContains( '[unsupportedsc', $email->get_html(), 'Expected shortcode to be stripped.' );
			$this->assertNotContains( '<iframe', $email->get_html(), 'Expected iframe to be stripped.' );
			$this->assertContains(
				$this->mail_data->img_src,
				$email->get_html(),
				'Expected image src to be present in the message.'
			);
			$this->assertNotContains(
				'<img',
				$email->get_text(),
				'Expected no image tag in the text version.'
			);
			$this->assertContains(
				'width="709"',
				$email->get_html(),
				'Expected image width to be adjusted to maximum 709.'
			);
			$this->assertNotContains(
				$this->mail_data->height_attribute,
				$email->get_html(),
				'Expected image height attribute to be stripped.'
			);
			$this->assertContains(
				'class="test retina"',
				$email->get_html(),
				'Expected retina class to be added to image.'
			);
			$this->assertContains(
				'incompatible',
				$email->get_html(),
				'Expected shortcode incompatible message to be included.'
			);
			$this->assertContains(
				'www-youtube-com',
				$email->get_html(),
				'Expected youtube incompatible class to be included.'
			);
			$this->assertContains(
				$this->mail_data->youtube_url,
				$email->get_html(),
				'Expected a link to the youtube video.'
			);
			$this->assertNotContains( '<noscript>', $email->get_html(), 'Expected noscript tags to be stripped.' );
			$this->assertContains(
				$this->mail_data->noscript_content,
				$email->get_html(),
				'Expected noscript content.'
			);

			$from_name = get_option( 'blogname' );

			if ( $email->get_to_address() == $this->mail_data->author_subscriber->user_email )
				$from_name .=  ' [' . $this->mail_data->author->display_name . ']';

			$this->assertEquals(
				$from_name,
				$email->get_from_name(),
				'Expected from name to be adjusted to subscribed object.'
			);

			$this->assertNotEmpty(
				$email->get_metadata(),
				'Expected the email to have command metadata.'
			);

			$this->assertEquals(
				Prompt_Enum_Message_Types::POST,
				$email->get_message_type(),
				'Expected post message type.'
			);
		}

	}

	function testClosedPostNotifications() {
		$this->mail_data->author = $this->factory->user->create_and_get( array( 'role' => 'author' ) );
		wp_set_current_user( $this->mail_data->author->ID );

		$site_subscriber = $this->factory->user->create_and_get();

		$post = $this->factory->post->create_and_get( array(
			'post_status' => 'draft',
			'post_author' => $this->mail_data->author->ID,
			'comment_status' => 'closed',
		) );

		$prompt_site = new Prompt_Site;

		$prompt_site->subscribe( $site_subscriber->ID );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_many' )
			->will( $this->returnCallback( array( $this, 'verifyClosedPostNotifications' ) ) );

		Prompt_Post_Mailing::send_notifications( $post );

		wp_set_current_user( 0 );
	}

	/**
	 * @param Prompt_Email[] $emails
	 */
	function verifyClosedPostNotifications( array $emails ) {

		$this->assertCount( 1, $emails, 'Expected one notification email.' );

		$this->assertEquals(
			$this->mail_data->author->user_email,
			$emails[0]->get_from_address(),
			'Expected from address to be the post author.'
		);

		$this->assertEmpty(
			$emails[0]->get_metadata(),
			'Expected the email to have NO command metadata.'
		);
	}

	function testExcerptPostNotifications() {
		$this->mail_data->author = $this->factory->user->create_and_get( array( 'role' => 'author' ) );
		wp_set_current_user( $this->mail_data->author->ID );

		$site_subscriber = $this->factory->user->create_and_get();

		$post = $this->factory->post->create_and_get( array(
			'post_status' => 'draft',
			'post_author' => $this->mail_data->author->ID,
		) );

		$prompt_site = new Prompt_Site;

		$prompt_site->subscribe( $site_subscriber->ID );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_many' )
			->will( $this->returnCallback( array( $this, 'verifyClosedPostNotifications' ) ) );

		$_POST['post_ID'] = $post->ID;
		$_POST['prompt_excerpt_only'] = 1;

		Prompt_Post_Mailing::send_notifications( $post );

		wp_set_current_user( 0 );
	}

	function testChunking() {

		$subscriber_ids = $this->factory->user->create_many( 30 );

		$this->mail_data->prompt_post = new Prompt_Post( $this->factory->post->create() );

		$site = new Prompt_Site();
		foreach ( $subscriber_ids as $id ) {
			$site->subscribe( $id );
		}

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_many' )
			->will( $this->returnValue( true ) );

		$this->mail_data->verified_chunk_event = false;
		add_filter( 'schedule_event', array( $this, 'verifyChunkEvent' ) );
		Prompt_Post_Mailing::send_notifications( $this->mail_data->prompt_post->get_wp_post() );
		remove_filter( 'schedule_event', array( $this, 'verifyChunkEvent' ) );

		$this->assertTrue( $this->mail_data->verified_chunk_event, 'Expected another mailing to be scheduled.' );
	}

	function verifyChunkEvent( $event ) {

		if ( 'prompt/post_mailing/send_notifications' != $event->hook )
			return $event;

		$this->assertEquals( $this->mail_data->prompt_post->id(), $event->args[0] );

		$unsent_ids = $this->mail_data->prompt_post->unsent_recipient_ids();
		$this->assertEquals(
			implode( '', $unsent_ids ),
			$event->args[1],
			'Expected unsent post IDs signature as second event argument.'
		);

		$this->mail_data->verified_chunk_event = true;

		// No need to actually schedule the event
		return false;
	}

}
