<?php

class JetpackPostRenderingModifierTest extends WP_UnitTestCase {

	function testGalleryStripping() {

		$post = $this->factory->post->create_and_get( array( 'post_content' => '[gallery type="square"]' ) );

		$this->factory->attachment->create_object( 'image.jpg', $post->ID, array(
			'post_mime_type' => 'image/jpeg',
			'post_type' => 'attachment',
		) );

		$query = new WP_Query( array( 'p' => $post->ID ) );
		$query->have_posts();
		$query->the_post();

		$modifier = new Prompt_Jetpack_Post_Rendering_Modifier();

		$modifier->setup();

		$this->assertNotContains(
			'gallery',
			apply_filters( 'the_content', $post->post_content ),
			'Expected no gallery shortcode output.'
		);

		$modifier->reset();

		$this->assertContains(
			'gallery',
			apply_filters( 'the_content', $post->post_content ),
			'Expected gallery shortcode output.'
		);

	}

}
