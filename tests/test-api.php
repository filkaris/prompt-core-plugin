<?php

class ApiText extends Prompt_MockMailerTestCase {

	function testSubscribeNew() {

		$subscriber_data = array(
			'email_address' => 'test@example.tld',  // Required
			'first_name'    => 'Example',           // Optional
			'last_name'     => 'User',              // Optional
		);

		$this->mail_data->subscriber_data = $subscriber_data;

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_one' )
			->will( $this->returnCallback( array( $this, 'verifyNewSubscriberEmail' ) ) );

		$status = Prompt_Api::subscribe( $subscriber_data );

		$this->assertEquals( Prompt_Api::OPT_IN_SENT, $status );

		$this->assertFalse(
			get_user_by( 'email', $subscriber_data['email_address'] ),
			'Expected no user with test email address.'
		);
	}

	function verifyNewSubscriberEmail( Prompt_Email $email ) {

		$this->assertEquals(
			$this->mail_data->subscriber_data['email_address'],
			$email->get_to_address(),
			'Expected email to new subscriber.'
		);

	}

	function testSubscribeUser() {

		$user = $this->factory->user->create_and_get();
		$this->mail_data->user = $user;

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_one' )
			->will( $this->returnCallback( array( $this, 'verifyUserSubscriberEmail' ) ) );

		$status = Prompt_Api::subscribe( array( 'user_email' => $user->user_email ) );

		$this->assertEquals( Prompt_Api::CONFIRMATION_SENT, $status );

	}

	function verifyUserSubscriberEmail( Prompt_Email $email ) {

		$this->assertEquals(
			$this->mail_data->user->user_email,
			$email->get_to_address(),
			'Expected email to user subscriber.'
		);

		$this->assertEquals(
			Prompt_Enum_Message_Types::SUBSCRIPTION,
			$email->get_message_type(),
			'Expected subscription message type.'
		);
	}

	function testSubscribeExisting() {
		$user = $this->factory->user->create_and_get();

		$site = new Prompt_Site();
		$site->subscribe( $user->ID );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->never() )->method( 'send_one' );

		$status = Prompt_Api::subscribe( array( 'email_address' => $user->user_email ) );

		$this->assertEquals( Prompt_Api::ALREADY_SUBSCRIBED, $status );
	}

	function testSubscribeInvalid() {

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->never() )->method( 'send_one' );

		$status = Prompt_Api::subscribe( array( 'email_address' => 'wtf' ) );

		$this->assertEquals( Prompt_Api::INVALID_EMAIL, $status );
	}

	function testUnsubscribe() {
		$user = $this->factory->user->create_and_get();
		$this->mail_data->user = $user;

		$site = new Prompt_Site();
		$site->subscribe( $user->ID );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_one' )
			->will( $this->returnCallback( array( $this, 'verifyUserSubscriberEmail' ) ) );

		$status = Prompt_Api::unsubscribe( $user->user_email );

		$this->assertEquals( Prompt_Api::CONFIRMATION_SENT, $status );
	}

	function testNeverSubscribed() {

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->never() )->method( 'send_one' );

		$status = Prompt_Api::unsubscribe( 'wtf' );

		$this->assertEquals( Prompt_Api::NEVER_SUBSCRIBED, $status );
	}

	function testAlreadyUnsubscribed() {
		$user = $this->factory->user->create_and_get();

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->never() )->method( 'send_one' );

		$status = Prompt_Api::unsubscribe( $user->user_email );

		$this->assertEquals( Prompt_Api::ALREADY_UNSUBSCRIBED, $status );
	}
}