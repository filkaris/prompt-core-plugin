<?php

class Prompt_MockMailerTestCase extends WP_UnitTestCase {

	/** @var Prompt_Mailer */
	protected $mock_mailer;
	/** @var  object */
	protected $mail_data;
	/** @var  string Email transport mailer was initialized with */
	protected $transport;

	function setUp() {
		parent::setUp();

		$this->mock_mailer = null;
		$this->mail_data = new stdClass();

		add_filter( 'prompt/make_mailer', array( $this, 'get_mock_mailer' ), 10, 2 );
	}

	function tearDown() {
		parent::tearDown();

		remove_filter( 'prompt/make_mailer', array( $this, 'get_mock_mailer' ), 10, 2 );
	}

	function get_mock_mailer( $mailer, $transport ) {
		$this->transport = $transport;
		return $this->mock_mailer;
	}

}