<?php

class CommentMailingTest extends Prompt_MockMailerTestCase {

	protected $user;
	protected $post;

	public function setUp() {
		parent::setUp();

		$this->post = $this->factory->post->create_and_get();
		$this->user = $this->factory->user->create_and_get();

	}

	function testSendRejectedNotification() {
		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_one' )
			->with( $this->objectHasAttribute( 'subject' ) )
			->will( $this->returnCallback( array( $this, 'verifyRejectedNotification' ) ) );

		Prompt_Comment_Mailing::send_rejected_notification( $this->user->ID, $this->post->ID );
	}

	function verifyRejectedNotification( Prompt_Email $email ) {
		$this->assertEquals( $this->user->user_email, $email->get_to_address(), 'Expected email to commenter only.' );
		$this->assertContains( $this->post->post_title, $email->get_subject(), 'Expected subject to contain post title.' );
		$this->assertEquals(
			Prompt_Enum_Message_Types::ADMIN,
			$email->get_message_type(),
			'Expected admin message type.'
		);
		return true;
	}

	function testSendRejectedNotificationForNonExistentPost() {
		$non_existent_post_id = -1;

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_one' )
			->with( $this->objectHasAttribute( 'subject' ) )
			->will( $this->returnCallback( array( $this, 'verifyRejectedNotificationForNonExistentPost' ) ) );

		Prompt_Comment_Mailing::send_rejected_notification( $this->user->ID, $non_existent_post_id );
	}

	function verifyRejectedNotificationForNonExistentPost( Prompt_Email $email ) {
		$this->assertEquals( $this->user->user_email, $email->get_to_address(), 'Expected email to commenter only.' );
		$this->assertContains( 'deleted post', $email->get_subject(), 'Expected subject to contain "deleted post".' );
		$this->assertEquals(
			Prompt_Enum_Message_Types::ADMIN,
			$email->get_message_type(),
			'Expected admin message type.'
		);
		return true;
	}

	function testCommentAddSubscription() {
		$prompt_post = new Prompt_Post( $this->factory->post->create() );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_one' )
			->will( $this->returnCallback( array( $this, 'verifyCommentSubscriberNotification' ) ) );

		 $this->mail_data->comment = $this->factory->comment->create_and_get( array(
			'comment_post_ID' => $prompt_post->id(),
		 ) );

		// Mock a stored response to the comment subscription form
		add_comment_meta( $this->mail_data->comment->comment_ID, Prompt_Comment_Form_Handling::SUBSCRIBE_CHECKBOX_NAME, 1 );

		$mock_comment_mailer = $this->getMock(
			'Prompt_Comment_Mailer',
			array( 'send_notifications' ),
			array( $this->mail_data->comment )
		);
		$mock_comment_mailer->expects( $this->once() )->method( 'send_notifications' );

		Prompt_Comment_Mailing::send_notifications( $this->mail_data->comment->comment_ID, '', $mock_comment_mailer );

		$this->assertEmpty(
			get_user_by( 'email', $this->mail_data->comment->comment_author_email ),
			'Expected no user with the commenter email address.'
		);
	}

	function verifyCommentSubscriberNotification( Prompt_Email $email ) {
		$this->assertEquals( $this->mail_data->comment->comment_author_email, $email->get_to_address() );
		$this->assertEquals(
			Prompt_Enum_Message_Types::SUBSCRIPTION,
			$email->get_message_type(),
			'Expected admin message type.'
		);
	}

}