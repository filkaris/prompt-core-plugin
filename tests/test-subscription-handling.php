<?php

class SubscriptionHandlingTest extends WP_UnitTestCase {

	function testCreateSubscribableSite() {
		$site = Prompt_Subscribing::make_subscribable();

		$this->assertInstanceOf( 'Prompt_Site', $site );
	}

	function testCreateSubscribablePost() {
		$post = $this->factory->post->create_and_get();

		$prompt_post = Prompt_Subscribing::make_subscribable( $post );

		$this->assertInstanceOf( 'Prompt_Post', $prompt_post );
		$this->assertEquals( $post->ID, $prompt_post->id() );
	}

	function testCreateSubscribableUser() {
		$user = $this->factory->user->create_and_get();

		$prompt_user = Prompt_Subscribing::make_subscribable( $user );

		$this->assertInstanceOf( 'Prompt_User', $prompt_user );
		$this->assertEquals( $user->ID, $prompt_user->id() );
	}

	function testGetSubscribableClasses() {
		$classes = Prompt_Subscribing::get_subscribable_classes();

		$this->assertCount( 3, $classes );
		$this->assertContains( 'Prompt_Site', $classes );
		$this->assertContains( 'Prompt_Post', $classes );
		$this->assertContains( 'Prompt_User', $classes );
	}


}
