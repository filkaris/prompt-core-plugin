<?php

class OptionsPageTest extends Prompt_MockMailerTestCase {

	/** @var Prompt_Admin_Options_Page $page */
	protected $page;

	function setUp() {
		parent::setUp();
		add_action( 'wp_feed_options', array( $this, 'disableFetchFeed' ) );
	}

	function disableFetchFeed( $feed ) {
		$feed->set_feed_url( '' );
	}


	function testPageHead() {
		$page = new Prompt_Admin_Options_Page( __FILE__, Prompt_Core::$options );

		$page->page_head();

		$this->assertTrue( wp_style_is( 'prompt-admin', 'enqueued' ), 'Expected admin styles to be enqueued.' );
		$this->assertTrue( wp_style_is( 'prompt-jmetro', 'enqueued' ), 'Expected jMetro to be enqueued.' );
		$this->assertTrue( wp_script_is( 'prompt-options-page', 'enqueued' ), 'Expected Prompt admin script to be enqueued.' );
	}

	function testTabFormHandler() {
		$page = new Prompt_Admin_Options_Page( __FILE__, Prompt_Core::$options );

		$mock_tab = $this->getMockBuilder( 'Prompt_Admin_Options_Tab' )
			->disableOriginalConstructor()
			->setMethods( array( 'slug', 'form_handler' ) )
			->getMock();

		$slug = 'test';
		$mock_tab->expects( $this->once() )->method( 'slug' )->will( $this->returnValue( $slug ) );
		$mock_tab->expects( $this->once() )->method( 'form_handler' );

		$page->add_tab( $mock_tab );

		$_POST['tab'] = $slug;

		$page->page_loaded();
	}

	function testNoKeyContent() {
		Prompt_Core::$options->set( 'prompt_key', '' );

		$page = new Prompt_Admin_Options_Page( __FILE__, Prompt_Core::$options );
		ob_start();
		$page->page_content();
		$content = ob_get_clean();

		$this->assertContains( 'id="prompt_key"', $content );
		$this->assertNotContains( 'id="prompt-tabs"', $content );

		Prompt_Core::$options->reset();
	}

	function testPageContent() {
		$key = 'test';
		Prompt_Core::$options->set( 'prompt_key', $key );
		$mock_page = $this->getValidKeyMock( $key );

		ob_start();
		$mock_page->page_content();
		$content = ob_get_clean();

		$this->assertContains( "id=\"prompt-tabs\"", $content );

		Prompt_Core::$options->reset();
	}

	function testBugReport() {
		$user_id = $this->factory->user->create();
		wp_set_current_user( $user_id );

		$_POST['error_alert'] = true;
		$_POST['submit_errors'] = true;

		$mock_page = $this->getMock(
			'Prompt_Admin_Options_Page',
			array( 'check_args' ),
			array( false, Prompt_Core::$options, null, array() )
		);
		$mock_page->expects( $this->never() )
			->method( 'form_handler' );

		$this->mock_mailer = $this->getMock( 'Prompt_Mailer' );
		$this->mock_mailer->expects( $this->once() )
			->method( 'send_one' )
			->with( $this->objectHasAttribute( 'subject' ) )
			->will( $this->returnCallback( array( $this, 'verifyBugReportEmail' ) ) );

		$mock_page->page_loaded();

		$dismissed_time = get_user_meta( $user_id, Prompt_Admin_Options_Page::DISMISS_ERRORS_META_KEY, true );

		$this->assertGreaterThan( time() - 60*60, $dismissed_time, 'Expected a recent bug report time.' );

		wp_set_current_user( 0 );
	}

	function verifyBugReportEmail( Prompt_Email $email ) {
		$user = wp_get_current_user();
		$this->assertEquals(  $user->user_email, $email->get_from_address(), 'Expected report from the current user.' );
		$this->assertEquals(
			Prompt_Core::SUPPORT_EMAIL,
			$email->get_to_address(),
			'Expected the bug report to be sent to the support address.'
		);
		$this->assertFalse( $email->have_html(), 'Expected a non-html email.' );
		$this->assertEquals(
			Prompt_Enum_Message_Types::ADMIN,
			$email->get_message_type(),
			'Expected admin message type.'
		);
	}

	function testRedirect() {
		Prompt_Core::$options->set( 'prompt_key', '' );
		add_filter( 'wp_redirect', array( $this, 'checkRedirect' ) );

		$admin = $this->factory->user->create_and_get( array( 'role' => 'administrator' ) );
		wp_set_current_user( $admin->ID );

		$this->mail_data->redirect_url = '';
		$page = new Prompt_Admin_Options_Page( __FILE__, Prompt_Core::$options, null, array(), array() );
		$this->assertEquals( $page->url(), $this->mail_data->redirect_url, 'Expected to detect an auto load redirect.' );

		wp_set_current_user( 0 );
		remove_filter( 'wp_redirect', array( $this, 'checkRedirect' ) );
		Prompt_Core::$options->reset();
	}

	function checkRedirect( $url ) {
		$this->mail_data->redirect_url = $url;
	}

	function getValidKeyMock( $key ) {
		$mock_page = $this->getMock(
			'Prompt_Admin_Options_Page',
			array( 'validate_key', 'display_key_prompt' ),
			array( __FILE__, Prompt_Core::$options, null, array(), array() )
		);
		$mock_page->expects( $this->once() )
			->method( 'validate_key' )
			->will( $this->returnValue( $key ) );
		$mock_page->expects( $this->never() )->method( 'display_key_prompt' );

		return $mock_page;
	}


}
