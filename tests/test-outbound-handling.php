<?php

class OutboundHandlingTest extends WP_UnitTestCase {

	protected $schedule_post_id;
	protected $schedule_comment_id;
	protected $schedule_verified;
	protected $unapproved_moderation_triggered;
	protected $native_notification_addresses;

	function setUp() {
		parent::setUp();
		$this->schedule_verified = false;
		$this->schedule_comment_id = null;
		$this->schedule_post_id = null;
	}

	function checkPostNotificationSchedule( $event ) {

		if ( 'prompt/post_mailing/send_notifications' != $event->hook )
			return $event;

		$this->assertContains( $this->schedule_post_id, $event->args, 'Expected post ID in scheduled event args.' );
		$this->schedule_verified = true;

		return false;
	}

	function testPostNotificationSchedule() {

		add_filter( 'schedule_event', array( $this, 'checkPostNotificationSchedule' ) );

		$post = $this->factory->post->create_and_get( array( 'post_status' => 'draft' ) );
		$subscriber_id = $this->factory->user->create();

		$prompt_site = new Prompt_Site();
		$prompt_site->subscribe( $subscriber_id );

		$this->schedule_post_id = $post->ID;

		$post->post_status = 'publish';
		wp_update_post( $post );

		$this->assertTrue( $this->schedule_verified, 'Expected the post schedule_event action to fire.' );

		remove_filter( 'schedule_event', array( $this, 'checkPostNotificationSchedule' ) );
	}

	function checkCommentNotificationSchedule( $event ) {

		if ( 'prompt/comment_mailing/send_notifications' != $event->hook )
			return $event;

		$this->assertCount( 1, $event->args, 'Expected one event argument: comment ID.' );
		$this->schedule_comment_id = $event->args[0];

		return false;
	}

	function testCommentSchedule() {

		add_filter( 'schedule_event', array( $this, 'checkCommentNotificationSchedule' ) );

		$post_id = $this->factory->post->create();
		$prompt_post = new Prompt_Post( $post_id );
		$prompt_post->subscribe( $this->factory->user->create() );

		$comment_id = $this->factory->comment->create( array(
			'comment_post_ID' => $post_id,
		) );

		$this->assertEquals( $comment_id, $this->schedule_comment_id, 'Expected comment id in scheduled event args.' );

		remove_filter( 'schedule_event', array( $this, 'checkCommentNotificationSchedule' ) );
	}

	function checkNoPostNotificationSchedule( $event ) {

		$this->assertNotEquals(
			'prompt/post_mailing/send_notifications',
			$event->hook,
			'Expected no post notification to be scheduled.'
		);

		return $event;
	}

	function testPostNotificationMetaboxOverride() {

		add_filter( 'schedule_event', array( $this, 'checkNoPostNotificationSchedule' ) );

		$post = $this->factory->post->create_and_get( array( 'post_status' => 'draft' ) );
		$subscriber_id = $this->factory->user->create();

		$prompt_site = new Prompt_Site();
		$prompt_site->subscribe( $subscriber_id );

		$_POST['post_ID'] = $post->ID;
		$_POST['prompt_no_email'] = true;

		$post->post_status = 'publish';
		wp_update_post( $post );

		remove_filter( 'schedule_event', array( $this, 'checkNoPostNotificationSchedule' ) );
	}

	/**
	 * http://docs.gopostmatic.com/article/148-will-trashed-posts-be-mailed-when-i-restore-them
	 */
	function testPostNotificationTrashOverride() {

		add_filter( 'schedule_event', array( $this, 'checkNoPostNotificationSchedule' ) );

		$post = $this->factory->post->create_and_get( array( 'post_status' => 'trash' ) );
		$subscriber_id = $this->factory->user->create();

		$prompt_site = new Prompt_Site();
		$prompt_site->subscribe( $subscriber_id );

		$_POST['post_ID'] = $post->ID;

		$post->post_status = 'publish';
		wp_update_post( $post );

		remove_filter( 'schedule_event', array( $this, 'checkNoPostNotificationSchedule' ) );
	}

	function testCommentUnapprovedAction() {
		remove_action( 'transition_comment_status', array( 'Prompt_Outbound_Handling', 'action_transition_comment_status' ) );
		add_filter( 'comment_moderation_recipients', array( $this, 'checkUnapprovedNotification' ), 9, 2 );
		Prompt_Core::$options->set( 'enabled_message_types', array( Prompt_Enum_Message_Types::COMMENT_MODERATION ) );

		$post_id = $this->factory->post->create();
		$comment = $this->factory->comment->create_and_get( array( 'comment_post_ID' => $post_id ) );

		$comment->comment_approved = 0;
		wp_update_comment( (array)$comment );

		$this->assertTrue( $this->unapproved_moderation_triggered, 'Expected to detect a moderation email.' );

		Prompt_Core::$options->reset();
		remove_filter( 'comment_moderation_recipients', array( $this, 'checkUnapprovedNotification' ), 9 );
		add_action( 'transition_comment_status', array( 'Prompt_Outbound_Handling', 'action_transition_comment_status' ), 10, 3 );
	}

	function checkUnapprovedNotification( $addresses, $comment_id ) {
		$this->unapproved_moderation_triggered = true;
		// Prevent actual sending
		return array();
	}

	function testNativeCommentNotification() {
		remove_action( 'transition_comment_status', array( 'Prompt_Outbound_Handling', 'action_transition_comment_status' ) );
		add_filter( 'comment_notification_recipients', array( $this, 'checkNativeNotification' ), 20 );
		Prompt_Core::$options->set( 'auto_subscribe_authors', false );

		$author = $this->factory->user->create_and_get();
		$post_id = $this->factory->post->create( array( 'post_author' => $author->ID ) );
		$comment_id = $this->factory->comment->create( array( 'comment_post_ID' => $post_id ) );

		wp_notify_postauthor( $comment_id );

		$this->assertContains(
			$author->user_email,
			$this->native_notification_addresses,
			'Expected an author comment notification.'
		);

		Prompt_Core::$options->reset();
		remove_filter( 'comment_notification_recipients', array( $this, 'checkNativeNotification' ), 20 );
		add_action( 'transition_comment_status', array( 'Prompt_Outbound_Handling', 'action_transition_comment_status' ), 10, 3 );
	}

	function checkNativeNotification( $addresses ) {
		$this->native_notification_addresses = $addresses;
		// Prevent actual sending
		return array();
	}
}

