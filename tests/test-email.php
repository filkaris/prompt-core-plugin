<?php

class EmailTest extends WP_UnitTestCase {

	function test_email_defaults() {
		$email = new Prompt_Email( array( 'to_address' => 'test@prompt.vern.al' ) );
		$this->assertNotEmpty( $email->get_from_address(), 'Expected a default from address.' );
		$this->assertNotEmpty( $email->get_subject(), 'Expected a default subject.' );
	}

	function test_html_email() {
		$html_from_name = 'Test O&#039;Matic';
		$text_from_name = "Test O'Matic";
		$html_subject = 'Test Subject &amp; <em>Stuff</em>';
		$text_subject = 'Test Subject & Stuff';
		$test_fields = array(
			'to_address' => 'test@prompt.vern.al',
			'from_name' => $html_from_name,
			'from_address' => 'from@email.org',
			'subject' => $html_subject,
			'html' => '<h1>Test HTML Message</h1><p>Now that&apos;s <em>HTML</em>.</p>',
			'reply_name' => 'Reply Name',
			'reply_address' => 'reply@email.org',
			'message_type' => Prompt_Enum_Message_Types::ADMIN,
		);
		$email = new Prompt_Email( $test_fields );

		$this->assertEquals( $test_fields['to_address'], $email->get_to_address() );
		$this->assertEquals( $text_subject, $email->get_subject() );
		$this->assertContains( $test_fields['html'], $email->get_html() );
		$this->assertEquals( $text_from_name, $email->get_from_name() );
		$this->assertEquals( $test_fields['from_address'], $email->get_from_address() );
		$this->assertEquals( $test_fields['html'], $email->get_html() );
		$this->assertContains( 'Now that\'s *HTML*', $email->get_text() );
		$this->assertEquals( $test_fields['reply_name'], $email->get_reply_name() );
		$this->assertEquals( $test_fields['reply_address'], $email->get_reply_address() );
		$this->assertEquals( $test_fields['message_type'], $email->get_message_type() );
	}
}