<?php

class WebApiTest extends WP_Ajax_UnitTestCase {
	/** @var int */
	protected $_timestamp = null;
	/** @var string */
	protected $_token = null;
	/** @var string */
	protected $_signature = null;
	/** @var string */
	protected $_prompt_key = null;
	/** @var Prompt_Inbound_Messenger */
	protected $_mock_messenger = null;

	function get_mock_messenger() {
		return $this->_mock_messenger;
	}

	function setUp() {
		parent::setUp();
		$this->_timestamp = time();
		$this->_token = md5( date( 'c' ) );
		$this->_prompt_key = '9ca1bc2515c5ddb67a7dbe038e994c9c';
		Prompt_Core::$options->set( 'prompt_key', $this->_prompt_key );
		$this->_signature = hash_hmac( 'sha256', $this->_timestamp . $this->_token, $this->_prompt_key );

		add_filter( 'prompt/make_inbound_messenger', array( $this, 'get_mock_messenger' ) );

		// Trying to avoid triggering updates
		remove_filter( 'admin_init', '_maybe_update_core' );
		remove_filter( 'admin_init', '_maybe_update_themes' );
		remove_filter( 'admin_init', '_maybe_update_plugins' );
	}

	function tearDown() {
		parent::tearDown();
		Prompt_Core::$options->reset();
		remove_filter( 'prompt/make_inbound_messenger', array( $this, 'get_mock_messenger' ) );
	}

	function testReceivePullUpdates() {
		$this->_mock_messenger = $this->getMock( 'Prompt_Inbound_Messenger' );
		$this->_mock_messenger->expects( $this->once() )
			->method( 'pull_updates' )
			->will( $this->returnValue( true ) );

		$_POST['timestamp'] = $this->_timestamp;
		$_POST['token'] = $this->_token;
		$_POST['signature'] = $this->_signature;

		try {
			$this->_handleAjax( 'nopriv_prompt/pull-updates' );
		} catch ( WPAjaxDieStopException $e ) {
			unset( $e );
		}
	}

	function testReceivePing() {

		$_POST['timestamp'] = $this->_timestamp;
		$_POST['token'] = $this->_token;
		$_POST['signature'] = $this->_signature;

		try {
			$this->_handleAjax( 'nopriv_prompt/ping' );
		} catch ( WPAjaxDieContinueException $e ) {
			unset( $e );
		}

		$data = json_decode( $this->_last_response );

		$this->assertTrue( $data->success, 'Expected the standard WP success response.' );
	}

}