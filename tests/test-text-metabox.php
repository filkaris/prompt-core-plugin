<?php

class TextMetaboxTest extends WP_UnitTestCase {
	protected static $custom_text_name = 'prompt_custom_text';

	function tearDown() {
		wp_set_current_user( 0 );
	}

	function makeMetabox() {

		return new Prompt_Admin_Text_Metabox(
			'test_text_metabox',
			'Test Text Metabox',
			array( 'post_type' => array( 'post' ) )
		);

	}

	function makeLoggedInAuthorPost( $status = 'draft' ) {
		$author_id = $this->factory->user->create( array( 'role' => 'author' ) );
		wp_set_current_user( $author_id );
		$post = $this->factory->post->create_and_get( array( 'post_status' => $status, 'post_author' => $author_id ) );
		return $post;
	}

	function testDefaultDisplay() {

		$post = $this->makeLoggedInAuthorPost();
		$_GET['post'] = $post->ID;

		$box = $this->makeMetabox();

		ob_start();
		$box->display( $post );
		$content = ob_get_clean();

		$this->assertContains( '<pre', $content, 'Expected a pre tag in the content.' );
		$this->assertContains( 'prompt-customize-text', $content, 'Expected a customize button in the content.' );

		wp_set_current_user( 0 );
	}

	function testCustomDraftDisplay() {
		$post = $this->makeLoggedInAuthorPost();

		$custom_text = 'CUSTOM TEXT';
		$prompt_post = new Prompt_Post( $post );
		$prompt_post->set_custom_text( $custom_text );

		$_GET['post'] = $post->ID;

		$box = $this->makeMetabox();

		ob_start();
		$box->display( $post );
		$content = ob_get_clean();

		$this->assertContains( '<textarea', $content, 'Expected a textarea tag in the content.' );
		$this->assertContains( $custom_text, $content, 'Expected the custom text in the content.' );
	}

	function testCustomPublishedDisplay() {
		$post = $this->makeLoggedInAuthorPost( 'publish' );

		$custom_text = 'CUSTOM TEXT';
		$prompt_post = new Prompt_Post( $post );
		$prompt_post->set_custom_text( $custom_text );

		$_GET['post'] = $post->ID;

		$box = $this->makeMetabox();

		ob_start();
		$box->display( $post );
		$content = ob_get_clean();

		$this->assertContains( '<pre', $content, 'Expected a pre tag in the content.' );
		$this->assertContains( $custom_text, $content, 'Expected the custom text in the content.' );
	}

	function testSave() {
		$post = $this->makeLoggedInAuthorPost();

		$custom_text = 'CUSTOM TEXT';
		$_POST[self::$custom_text_name] = $custom_text;
		$_POST['post_ID'] = $post->ID;
		$_POST['action'] = 'editpost';

		$box = $this->makeMetabox();

		$box->_save_post( $post->ID, $post );

		$prompt_post = new Prompt_Post( $post );

		$this->assertEquals( $custom_text, $prompt_post->get_custom_text(), 'Expected custom text to be set.' );
	}
}